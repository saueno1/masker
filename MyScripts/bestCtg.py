#!/usr/bin/python
'''
Author	:	Fu-Jin Wei
Date	:	Sep. 14, 2017 (original on Sep.5, 2017)
command	:	bestCtg.py
Descript	:
  Read and store all BLAST result of orthologous candidate genes against all
libraries. From these result, find the best representive contig for each gene.
Output	:
  stdout, three-columns table : Candidate gene, library name, contig name and strand.
changlog:
Sep.14,2017 : add strand
'''
import sys

libs=[ "S1s","S2s","S3s","S4s","S5s","S6s","Ooi-7","S1NK4","S5HK7","S8HK5" ]
OCGAln={}
for l in libs :
  with open("OrthCand_in_"+l+".outfmt6_sel1.txt", "r") as f :
    for r in f :
       A=r.rstrip("\n").split("\t")
       A[1]=l+"_"+A[1]
       if A[0] not in OCGAln :
         OCGAln[A[0]]=A[1:]
       else :
         B=OCGAln[A[0]]
         if float(A[11]) > float(B[10]) or ( A[11] == B[10] and int(A[3]) > int(B[2]) ) or ( A[11] == B[10] and A[3] == B[2] and float(A[2]) > float(B[1]) ) :
           OCGAln[A[0]]=A[1:]

for k,v in sorted(OCGAln.iteritems()) :
   strand = "+" if v[7] < v[8] else "-"
   print "\t".join([k]+v[0].split("_",1)+[strand])
