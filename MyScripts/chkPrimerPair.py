#!/usr/bin/python
import sys,gzip

GtoP={}
with gzip.open(sys.argv[1],"r") as BLTf :
    for l in BLTf :
       BArr = l.rstrip("\n").split("\t")
       G=BArr[1]
       ( P , O ) = BArr[0].split("_")
       if G not in GtoP  or P not in GtoP[G] :
         GtoP[G] = { P : { O : 1 } }
       else :
         GtoP[G][P][O]=1

for g in GtoP :
   pn=0
   for p in GtoP[g] :
      if "F" in GtoP[g][p] and "R" in GtoP[g][p] :
        print("\t".join([g,p]))

