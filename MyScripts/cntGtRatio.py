#!/usr/bin/python
import sys,gzip,re

DNAs="ATCG"

MPf=sys.argv[1]
MPs=[]
with gzip.open(MPf,"r") as f:
  for r in f:
    RA=r.rstrip("\n").split()
    RefID=RA[0]
    RefPos=RA[1]
    RefCode=RA[2]
    GtS=[]
    for i in range(3,len(RA),3) :
       GtA={ 'A':0, 'T':0, 'C':0, 'G':0 }
       RA[i+1]=RA[i+1].upper()
       asRf=re.findall('[\.\,]',RA[i+1])
       GtA[RefCode]=len(asRf)
       for c in sorted(GtA) :
         if c != RefCode :
           GtA[c]=len(re.findall(c,RA[i+1]))
       dp=sum(GtA.values())
       if dp > 0 :
         GtS.append( str(float(GtA[RefCode])/dp) )
       else :
         GtS.append("0")
    print("\t".join(RA[:3]+GtS))
