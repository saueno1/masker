#!/usr/bin/python
'''
   Scan mpileup code of all libraries.
   Only for SNP.
   Assume the SNP only two type, either the same as reference or not.
   checkUniq.py <bed file.gz> <mpileup file.txt> <skip number from 0>
'''
import sys,gzip

minD=5
libN=10
minLR=0.2
if len(sys.argv) < 2 :
  print 'checkUniq.py <bed.gz> <mpileup.txt.gz> <skip number from 0>'
  sys.exit(0)

bedF=sys.argv[1]
fnm=sys.argv[2]
sk=sys.argv[3].split(",")

bedR={}
with gzip.open(bedF,'r') as b :
    for l in b :
       a=l.rstrip("\n").split("\t")
       t=a[3].split("_")
       if a[0] not in bedR :
          t[3]=t[3].split(",")[0]
          bedR[a[0]] = { int(a[2]) : [ t[1], t[3] ] }
       else :
          bedR[a[0]][int(a[2])] = [ t[1], t[3] ]

with gzip.open(fnm,'r') as f :
    for l in f :
       TU=libN
       RA=l.rstrip("\n").split("\t")
       if RA[0] not in bedR or int(RA[1]) not in bedR[RA[0]] :
          continue
       for lbn in range(libN) :
          if lbn not in sk :
             mp=RA[lbn*3+4].upper()
             #TU = int(RA[lbn*3]) if int(RA[lbn*3]) < TU else TU
             #print RA[0],RA[1],mp
             if int(RA[lbn*3+3]) < minD :
                TU-=1
                #print "sample "+str(lbn)+" is too thin at this site, "+RA[0]+":"+RA[1]
                #break
             #elif len(mp) > len(mp.replace(bedR[RA[0]][int(RA[1])][1],"")) :
             elif bedR[RA[0]][int(RA[1])][1].upper() in mp :
                TU=0
                #print "sample "+str(lbn)+" have ALT allele at this site, "+RA[0]+":"+RA[1]
                break
       if TU > 0 and float(TU)/float(libN) >= minLR :
          print "\t".join([RA[0],RA[1],str(TU)+"/"+str(libN)])
       bedR[RA[0]].pop(int(RA[1]), None)
       if len(bedR[RA[0]]) == 0  :
          bedR.pop(RA[0],None)
    #print "There is "+str(len(bedR))+" left in the BED list."
