#!/usr/bin/python
'''
Author	: Fu-Jin Wei
Date	: Oct. 6, 2017
command	: BLASTrltToTaxa.py BLASTresult.txt.gz
require	:
  There must at least 16 columns in BLASTresult.txt.gz. 16th columns is taxid.
'''
import sys,gzip

KCf="/lfs/maskerwei/sequences/categories.dmp.gz"
SpTxf="/lfs/maskerwei/sequences/Taxid_Euphyllophyta.txt.gz"
BLRf=sys.argv[1]
recHiTx="Euphyllophyta"
recHTCode=recHiTx[:3]

KC={}
SpTx=[]
clsTx={}
with gzip.open(KCf, "r") as f :
   for l in f :
     k,r,t = l.rstrip("\n").split("\t")
     KC[t]=[k,r]
     if r not in KC :
        KC[r]=[k,r]
with gzip.open(SpTxf, "r") as f :
   for l in f:
     SpTx.append(l.rstrip("\n"))

with gzip.open(BLRf, "r") as f :
   for l in f :
     lA = l.rstrip("\n").split("\t")
     if len(lA) < 16 :
        exit("The column number not enough.\n"+lA)
     Ctg=lA[0]
     HtTx=lA[15]
     if Ctg in clsTx and clsTx[Ctg][1]==recHiTx :
        continue
     if HtTx in KC :
       if KC[HtTx][0] == "E" :
         if HtTx in SpTx :
           clsTx[Ctg]=[Ctg,recHiTx]+KC[HtTx]
         elif Ctg not in clsTx or clsTx[Ctg][2] != "E" :
           clsTx[Ctg]=[Ctg,"Eukaryota"]+KC[HtTx]
       elif Ctg not in clsTx :
         clsTx[Ctg]=[Ctg,"notEukaryota"]+KC[HtTx]
     else :
        clsTx[Ctg]=[Ctg,"unclassified","0",HtTx]

for v in clsTx.values() :
   print("\t".join(v))
