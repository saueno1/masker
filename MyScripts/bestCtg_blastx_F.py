#!/usr/local/python-2.7.3/bin/python
'''
Author	:	Fu-Jin Wei
Date	:	Sep. 14, 2017 (original Sep. 7, 2017)
command	:	bestCtg_blastx.py (previous : bestCtg2.py)
Descript	:
  Modified from bestCtg.py (Sep.5,2017).
  Read and store all BLAST result of orthologous candidate genes against all
libraries. From these result, find the best representive contig for each gene.
And the contigs by gene, end of "_g", as unit, to represent one gene only.
Output	:
  stdout, three-columns table : Candidate gene, library name, contig name and contig strand.
Changlog :
Sep.14,2017 : add "strand"
'''
import sys,gzip

minAA=20
minR=0.5
libs=[ "S1s","S2s","S3s","S4s","S5s","S6s","Ooi-7","S1NK4","S5HK7","S8HK5" ]

GAln={} # Orthlogous gene name and a list, that contain basic 11 columns (without gene name)
CGAln={} # Contigs name end in "_g" and a list, that contain basic 12 columns.
for l in libs :
  with gzip.open("Trinity_"+l+"-SwProt.outfmt6_sel1.txt.gz", "rb") as f :
    for r in f :
       A=r.rstrip("\n").split("\t")
       A[0]=l+"_"+A[0]
       if A[1] not in GAln :
         GAln[A[1]]=[A[0]]+A[2:]
       else :
         B=GAln[A[1]]
         if float(A[11]) > float(B[10]) or ( A[11] == B[10] and int(A[3]) > int(B[2]) ) or ( A[11] == B[10] and A[3] == B[2] and float(A[2]) > float(B[1]) ) :
           GAln[A[1]]=[A[0]]+A[2:]

for k,v in sorted(GAln.iteritems()) :
   CtgG = v[0][:v[0].find("_i")]
   if CtgG not in CGAln or float(v[10]) > float(CGAln[CtgG][11]) :
      CGAln[CtgG]=[k]+v

for k,v in sorted(CGAln.iteritems()) :
   strand="+" if v[6] < v[7] else "-"
   if float(v[2])*float(v[3])/100 < minAA and float(v[3])/float(v[13]) < minR :
      continue
   print "\t".join([v[0]]+v[1].split("_",1)+[strand])
