#!/usr/bin/python
import sys,re

NIDGa={}
NIDIa={}
with open(sys.argv[1],"r") as NIDf :
    for l in NIDf :
       a=l.rstrip("\n").split("\t")
       NIDIa[a[0]]=a[1]
       GID=a[0][:a[0].find("_i")]
       if GID not in NIDGa :
          NIDGa[GID]=a[1][:a[1].find("-")]

with open(sys.argv[2],"r") as bePf :
    for l in bePf :
       l=l.rstrip("\n")
       m=re.findall('>([HKNOSios0-9\-]+_TRINITY_DN[0-9]+_c[0-9]+_g[0-9]+_[im][0-9]+)',l)
       g=re.findall('>([HKNOSios0-9\-]+_TRINITY_DN[0-9]+_c[0-9]+_g[0-9]+)',l)
       if len(m) > 0 :
          for i in m :
             if '_i' in i :
                l=l.replace(i,NIDIa[i])
       if len(g) > 0 :
          l=l.replace(g[0],NIDGa[g[0]])
       print l
