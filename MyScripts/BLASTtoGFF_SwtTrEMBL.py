#!/usr/bin/python
# BLASTtoGFF.py BLASTresult.gz 
# read gzip BLAST result, then parse to GFF format 
# None filter in this process. Expect 2 subject per query at most.
import sys
import gzip

minR=0.4
GenMatch={}

print "##gff-version 3"

with gzip.open(sys.argv[1],"rb") as BLTF :
   for bl in BLTF :
      bl=bl.rstrip("\n")
      blA=bl.split("\t")
      CtgID=blA[0]
      CtgGID=CtgID[:CtgID.find("_i")]
      if CtgGID not in GenMatch :
          GenMatch[CtgGID]=1
      else :
          GenMatch[CtgGID]+=1
      RID=CtgGID+"_m"+str(GenMatch[CtgGID])
      '''for blastx ( DNA as query, protein as subject),
         query's orientation chaged to match subject
      '''
      if float(blA[6]) < float(blA[7]) :
         Strand="+"
         Qst=blA[6]
         Qed=blA[7]
      else :
         Strand="-"
         Qst=blA[6]
         Qed=blA[7]
      OSSt=blA[14].find(" OS=")
      GeneName=blA[14][:OSSt]
      PESt=blA[14].find(" PE=")
      SVSt=blA[14].find(" SV=")
      if blA[14].find(" GN=") > 0 :
         OsEd=blA[14].find(" GN=")
         GN=blA[14][OsEd+4:PESt]
      else :
         OsEd=PESt
         GN="x"
      OS=blA[14][OSSt+4:OsEd]
      PE=blA[14][PESt+4:SVSt]
      SV=blA[14][SVSt+4:]
      attr=["ID="+RID,"Name=similart to "+GeneName]
      attr+=["Target="+blA[1]+" "+blA[8]+" "+blA[9]+" "+Strand]
      attr+=["Identity="+blA[2],"HSP="+blA[3],"MissMatch="+blA[4],"Gap="+blA[5],"EValue="+blA[10]]
      attr+=["ContigLength="+blA[12],"GeneLength="+blA[13],"OS="+OS,"GN="+GN,"PE="+PE,"SV="+SV]
      print "\t".join([blA[0],"blastx","similarity",Qst,Qed,blA[11],Strand,".",";".join(attr)])
