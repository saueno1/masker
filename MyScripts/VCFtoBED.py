#!/usr/bin/python
import sys,gzip

withINDEL=False
minD=5
if len(sys.argv) < 4 :
  print "VCFtoBED.py VCF.gz REFname ALTname"
  sys.exit(0)

(VCFfn,REF,ALT) = sys.argv[1:5]

with gzip.open(VCFfn,"rb") as V :
    for vcfR in V :
       if vcfR[0]=="#" : continue
       vcfRA=vcfR.rstrip("\n").split("\t")
       infoA={}
       for x in vcfRA[7].split(";") :
          if x != "INDEL" :
            tmp=x.split("=")
            infoA[tmp[0]]=tmp[1]
          else :
            infoA["INDEL"]=True
       DP4=infoA["DP4"].split(",")
#       if int(DP4[-2])+int(DP4[-1]) >= minD and "INDEL" not in infoA :
       if int(DP4[-2])+int(DP4[-1]) >= minD :
          print "\t".join([vcfRA[0],str(int(vcfRA[1])-1),vcfRA[1],REF+"_"+vcfRA[3]+"_"+ALT+"_"+vcfRA[4]])
