#!/usr/local/Python-2.7.3/bin/python
import sys,re

if len(sys.argv) > 1 :
  inFa=open(sys.argv[1],"r")
else :
  inFa=sys.stdin
lSeq=[ "NullSeq", ""]
for FaLine in inFa :
  FaLine=FaLine.rstrip("\n")
  if FaLine[0] == ">" :
     if lSeq[0] != "NullSeq" :
        print("\t".join([lSeq[0],str(len(lSeq[1])),lSeq[1]]))
        lSeq[1]=""
     lSeq[0]=FaLine[1:].split()[0]
  else :
     lSeq[1]+=re.sub(r'[^ATGCUatgcuARNDCQEGHILKMFPOSUTWYVBZXJ]','',FaLine)
print("\t".join([lSeq[0],str(len(lSeq[1])),lSeq[1]]))
