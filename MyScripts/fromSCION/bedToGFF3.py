#!/usr/bin/python
import sys

inBED=sys.argv[1]

tp=( "mRNA", "five_prime_UTR", "CDS", "three_prime_UTR" )
Fsrc="predicted"


with open(inBED,"r") as inB :
    for rl in inB :
       rlA=rl.rstrip("\n").split("\t")
       GId=rlA[0]
       CDSSt=str(int(rlA[1])+1)
       CDSEd=rlA[2]
       mRNAEd=rlA[3]
       print(" ".join(["##sequence-region",GId,"1",mRNAEd]))
#       print("\t".join([ GId, Fsrc, "mRNA", "1", mRNAEd, ".", "+", ".", "ID="+GId]))
       print("\t".join([ GId, Fsrc, "exon", "1", mRNAEd, ".", "+", ".", "Parent="+GId]))
#       print("\t".join([ GId, Fsrc, "five_prime_UTR", "1", str(int(CDSSt)-1), ".", "+", ".", "Parent="+GId ]))
       print("\t".join([ GId, Fsrc, "CDS", CDSSt, CDSEd, ".", "+", "0", "Parent="+GId+";product="+GId]))
#       print("\t".join([ GId, Fsrc, "three_prime_UTR", str(int(CDSEd)+1), mRNAEd, ".", "+", ".", "Parent="+GId ]))
     
