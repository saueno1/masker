#!/usr/bin/python
import sys

with open(sys.argv[1],"r") as gf :
    for l in gf :
       GfL=l.rstrip("\n").split("\t")
       if l[0] == "#" or GfL[2] != "mRNA" :
          continue
       Desc=dict(x.split("=") for x in GfL[8].split(";") )
       GId = Desc["Parent"] if "Parent" in Desc else Desc["Locus_id"]
       GId = GId[GId.find(":")+1:] if ":" in GId else GId
       TId = Desc["ID"][Desc["ID"].find(":")+1:] if ":" in Desc["ID"] else Desc["ID"]
       if "Note" in Desc :
          Ann = Desc["Note"]
       elif "full_name" in Desc :
          Ann = Desc["full_name"]
       elif "Dbxref" in Desc :
          Ann = Desc["Dbxref"]
       else :
          Ann = "unknown"
       print("\t".join([ GId, TId, Ann ]))
