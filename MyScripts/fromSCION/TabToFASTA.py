#!/usr/bin/python
import sys,subprocess

seqTabF=sys.argv[1]
with open( seqTabF, "r") as f :
   for l in f:
     A=l.rstrip("\n").split("\t")
     Qstr = "plus" if A[3] == "+" else "minus"
     cmd = "blastdbcmd -entry "+A[2]+" -strand "+Qstr+" -db Trinity_"+A[1]
     p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
     ( out, err) = p.communicate()
     out=out.replace(">TRINITY",">"+A[1]+"_TRINITY")
     ( SeqID,Seq )=out.split("\n",1)
     SeqID=SeqID.split()[0]
     Seq=''.join(Seq.split())
     print(SeqID+"\n"+Seq)
