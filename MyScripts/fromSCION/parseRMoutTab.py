#!/usr/bin/python
'''
Author	: Fu-Jin Wei @ FFPRI
Date	: Oct. 3, 2017
Command	: parseRMoutTab.py RepeatMask_out_table
Output	: Contig gene ID & TE type
'''
import sys

RMout=sys.argv[1]
TEhead=( "DN","LT","LI","SI","Re","RC" )

RMA = {}
with open(RMout,"r") as f :
  next(f) ; next(f); next(f)

  for r in f :
    rA=r.rstrip("\n").split()
    if rA[10][:2] not in TEhead :
       continue
    CgId=rA[4][:rA[4].find("_i")]
    if CgId not in RMA :
      RMA[CgId]=[ int(rA[0]), rA[4], rA[10] ]
    elif int(rA[0]) > RMA[CgId][0] :
      RMA[CgId]=[ int(rA[0]), rA[4], rA[10] ]

for k,v in RMA.iteritems() :
   print "\t".join([k, rA[10]])
  #  print rA[4][:rA[4].find("_i")],rA[10]
