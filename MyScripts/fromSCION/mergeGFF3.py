#!/usr/bin/python
import sys

newGFF=['','','']
GF="X"
with sys.stdin as gffF :
    for gfl in gffF :
       gfl=gfl.rstrip('\n')
       if "##sequence-region" in gfl :
          newGFF[0]+=gfl+"\n"
          GF="G"
       elif "##FASTA" in gfl :
          GF="F"
       elif "##gff-version" in gfl :
          GF="X"
       else :
          if GF=="G" :
            newGFF[1]+=gfl+"\n"
          if GF=="F" :
            newGFF[2]+=gfl+"\n"
       
print newGFF[0],newGFF[1],"##FASTA\n",newGFF[2]
