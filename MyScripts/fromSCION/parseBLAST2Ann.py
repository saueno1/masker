#!/usr/bin/python
import sys
import gzip

minR=0.4
BLASTrltD={}
BLASTrltF=gzip.open(sys.argv[1],"rb")
for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    if Rec[0] not in BLASTrltD :
       BLASTrltD[Rec[0]]=[ Rec[1:], [ float(Rec[2]),int(Rec[3]) ] ]
    else :
       if BLASTrltD[Rec[0]][0][0] == Rec[1] :
          BLASTrltD[Rec[0]][1][1] += int(Rec[3])
          if BLASTrltD[Rec[0]][1][0] < Rec[2] : 
             BLASTrltD[Rec[0]][1][0] = Rec[2] 

for key,rec in BLASTrltD.iteritems()  :
    x=[key]+rec[0]+[str(rec[1][0]),str(rec[1][1])]
# If the joined record match to the condition, eg: sum length of HSP could explain over minR of subject length probably
    if float(x[16])/float(x[13]) >= minR :
# print these column: query ID, subject ID, sum length of HSP, query length, subject length, title of subject
       print("\t".join(x[:-2]))
#       print("\t".join([key,rec[0][0],str(rec[1][1]),rec[0][-3],rec[0][-2],rec[0][12],rec[0][-1]]))
