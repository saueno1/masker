#!/usr/bin/python
import sys

if len(sys.argv) < 3 :
   sys.exit() 

OneSMax=3
BED={}
BEDf=open(sys.argv[1],"r")
for i, bedR in enumerate(BEDf) :
   bedR=bedR.rstrip("\n")
   if bedR[0]=="#" :
      continue
   ( Ctg, Pos0, Pos1, RA )=bedR.split("\t")
   if Ctg not in BED :
      BED[Ctg]={}
   BED[Ctg][int(Pos1)]=[ RA.split(",")[0], RA.split(",")[1] ]

MPFs=[]
for i in range(2,len(sys.argv)) :
   tmpF=open(sys.argv[i],"r")
   MPFs.append(tmpF)

for fp in MPFs :
   for i, l in enumerate(fp) :
      l=l.rstrip("\n")
      lA=l.split("\t")
      (Ctg,Pos,REF)=lA[:3]
      Pos=int(Pos)
      if Ctg in BED and Pos in BED[Ctg] :
        for j in range(3,len(lA[3:]),3) :
           if lA[j+1].upper().count(BED[Ctg][Pos][1]) > OneSMax :
              BED[Ctg].pop(Pos)
              break
            

for CtgB in sorted(BED) :
   for Pos,RAs in sorted(BED[CtgB].items()) :
      print("\t".join([CtgB,str(Pos-1),str(Pos),RAs[0]+","+RAs[1]]))
       

BEDf.close()
for fp in MPFs :
   fp.close()
