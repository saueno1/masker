#!/usr/bin/python
import sys
import gzip

minR=0.0
BLASTrltD={}
BLASTrltF=gzip.open(sys.argv[1],"rb")
for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    QId=Rec[0]
    SId=Rec[1]
    Qst=int(Rec[6])
    Qed=int(Rec[7])
    if int(Rec[8]) < int(Rec[9]) :
       Sst = int(Rec[8])
       Sed = int(Rec[9])
    else :
       Sst = int(Rec[9])
       Sed = int(Rec[8])
    if QId not in BLASTrltD :
       BLASTrltD[QId]={ SId : [ [ Qst,Qed,Sst,Sed ],Rec[12:] ] }
    else :
       if SId not in BLASTrltD[QId] :
          BLASTrltD[QId][SId]=[ [ Qst,Qed,Sst,Sed ],Rec[12:] ]
       else :
          Qst = Qst if Qst < BLASTrltD[QId][SId][0][0] else  BLASTrltD[QId][SId][0][0]
          Qed = Qed if Qed > BLASTrltD[QId][SId][0][1] else  BLASTrltD[QId][SId][0][1]
          Sst = Sst if Sst < BLASTrltD[QId][SId][0][2] else  BLASTrltD[QId][SId][0][2]
          Sed = Sed if Sed > BLASTrltD[QId][SId][0][3] else  BLASTrltD[QId][SId][0][3]
          BLASTrltD[QId][SId]=[ [ Qst,Qed,Sst,Sed ],Rec[12:] ]
          
for Q,SRec in sorted(BLASTrltD.iteritems())  :
   for S,rec in sorted(SRec.iteritems()) :
      HSPcov=rec[0][1]-rec[0][0]+1
      if HSPcov/float(rec[1][0]) >= minR :
         print "\t".join([ Q,S,str(HSPcov),str(HSPcov/float(rec[1][0]))])
