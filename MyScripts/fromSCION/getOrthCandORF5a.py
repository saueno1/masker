#!/usr/bin/python
# usage: getORF.py $gene
# Read prepared alignment summarized table and fasta file, Orth_$gene.lst & Orth_$gene.fa.
# Extract longest ORF and process multiple-alignment by calling "clutalw2"
# The output file : ORF in FASTA, multiple-alignment result in FASTA
# The std output : A VCF-like table of variant site by Ref.'s postion and genotype(0/1/2...)
#                  Also, include a summary table
# The functions, get_longest and get_orfs were copy from Patrick Maupin's reply on https://stackoverflow.com

import sys,re
import string
import subprocess

def revClSeq(dna) :
    complement = { 'A':'T', 'C':'G', 'G':'C', 'T':'A' }
    return ''.join([complement[base] for base in dna[::-1]])

StCodon, EdCodon = [re.compile(x) for x in 'ATG TAG|TGA|TAA'.split()]

revtrans = string.maketrans("ATGC","TACG")


codontable = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

VT={}
VSum={}

def get_longest(starts, ends):
    ''' Simple brute-force for now.  Optimize later...
        Given a list of start locations and a list
        of end locations, return the longest valid
        string.  Returns tuple (length, start position)

        Assume starts and ends are sorted correctly
        from beginning to end of string.
    '''
    results = {}
    # Use smallest end that is bigger than each start
    ends.reverse()
    for start in starts:
        for end in ends:
            if end > start and (end - start) % 3 == 0:
                results[start] = end + 3
    results = [(end - start, start) for
               start, end in results.iteritems()]
    return max(results) if results else (0, 0)

def get_orfs(dna):
    ''' Returns length, header, forward/reverse indication,
        and longest match (corrected if reversed)
    '''
    header, seqf = dna
    seqr = seqf[::-1].translate(revtrans)
    def readgroup(seq, group):
        return list(x.start() for x in group.finditer(seq))
    f = get_longest(readgroup(seqf, StCodon), readgroup(seqf, EdCodon))
    r = get_longest(readgroup(seqr, StCodon), readgroup(seqr, EdCodon))
    (length, index), s, direction = max((f, seqf, 'forward'), (r, seqr, 'reverse'))
    return length, header, direction, s[index:index + length]

def scanVar(MSA,RF):
    ''' Scan the multiple aligment result for detecting variant in Nuc by Nuc.
        The output format is a list inside of a dictionary, VT.
        It only contain variant site.
        VT = { AlnPos1 : [ RefPos, Ref, Alt, RefAA, AltAA, S1, S2, S3, S4... ],
               AlnPos2 : ...,
               AlnPos3 : ...,
               ...  
               AlnPosN : ... }
        MSAfa = { CtgSrc1 : [ CtgName1, CtgSeq1 ],
                  CtgSrc2 : [ CtgName2, CtgSeq2 ],
                  ...
                  CtgSrcN : [ CtgNameN, CtgSeqN ] }
    '''
    with open(MSA,'r') as MSAf :
        MSAfa = MSAf.read()
    MSAfa = [ x.split('\n',1) for x in MSAfa.split('>')]
    MSAfa = { x[0][:x[0].find("_")] : [ x[0],''.join(x[1].split()) ] for x in MSAfa if len(x) ==2 }
    SrcS = sorted(i for i in MSAfa)
    SrcS.remove(RF)

    VarType=[ 'NS', 'S', 'Fr', 'Ins', 'Del', 'ErStop' ]
    VSum={ x: { y: 0 for y in VarType } for x in SrcS }

    print "\t".join(["PosInFA","PosInRef",RF,"ALT","RefAA","AltAA"]+SrcS)

    RFseq=MSAfa[RF][1]
    StInRF=RFseq.find("ATG")
    EdInRF=RFseq.rfind(EdCodon.findall(RFseq)[-1])
    RfP=0

 #  Transform multiple aligment FASTA (MSAfa) to variant table (VT) 
    for Sp in range(0,EdInRF+3) :
        RfNuc=RFseq[Sp]
        if Sp == StInRF :
           RfP=1
        elif Sp >= StInRF and RFseq[Sp] != '-':
           RfP+=1
        elif Sp < StInRF :
           RfP=Sp-StInRF
        for CtgSrc in SrcS :
            CgNuc=MSAfa[CtgSrc][1][Sp]
            GTcul=SrcS.index(CtgSrc)+5
            if CgNuc != RfNuc :
               if Sp not in VT :
                  VT[Sp]=[ RfP, RfNuc, CgNuc,'.' ,'.' ]
                  for NRn in range(0,len(SrcS)) :
                      VT[Sp].extend('.')
                  VT[Sp][GTcul]='1'
               else :
                  ALTs=VT[Sp][2].split(",")
                  if CgNuc in ALTs :
                     VT[Sp][GTcul]=str(ALTs.index(CgNuc)+1)
                  else :
                     VT[Sp][2]=VT[Sp][2]+","+CgNuc
                     VT[Sp][GTcul]=str(len(ALTs)+1)

    InsEv={ x: set() for x in SrcS }
    DelEv={ x: set() for x in SrcS }
    for Sp,Vr in sorted(VT.iteritems()) :
        '''
        Working on "Detecting INDEL", now.
        Finished  "detecting synonymous or non-synonymous SNP".
        '''
        InIns = True if Vr[1] == '-' else False

        ALTs = [Vr[1]]+Vr[2].split(",")

        RfCdn={ x: RFseq[x] for x in findCdnPos(RFseq,Sp,Vr[0]) }
        VT[Sp][3]=codontable["".join([RfCdn[x] for x in sorted(RfCdn.keys())])]

        for Cul in range(5,len(Vr)) :
            GT=Vr[Cul] 
            if GT != "." and GT != "0":
               GTnuc=ALTs[int(GT)]
               if InIns and GTnuc != "-" :
                  if Sp in InsEv[SrcS[Cul-5]] :
                     trackIns = False
                     InsSp=Sp+3
                  else :
                     InsEv[SrcS[Cul-5]].add(Sp)
                     trackIns=True
                     InsSp=Sp+1
                  while trackIns :
                     if InsSp not in VT or VT[InsSp][1] != '-' :
                        trackIns=False
                        break
                     InsAlts=[VT[InsSp][1]]+VT[InsSp][2].split(",")
                     InsGT=VT[InsSp][Cul] if VT[InsSp][Cul] != '0' and VT[InsSp][Cul]!='.' else 0
                     if InsAlts[int(InsGT)] == '-' :
                        trackIns=False
                        break
                     else :
                        InsEv[SrcS[Cul-5]].add(InsSp)
                     InsSp+=1
                  if len(InsEv[SrcS[Cul-5]]) % 3 != 0 :
                     AltAA="Fr_Ins" 
                  else :
                     AltAA="Ins"
               elif InIns == False and GTnuc == '-' :
           #       trackDel=True if GT2NT(RFseq,Sp-1,VT[Sp-1],Cul-5) == "-" else False
                  if Sp in DelEv[SrcS[Cul-5]] :
                     trackDel = False
                     DelSp=Sp+3
                  else :
                     trackDel = True
                     DelEv[SrcS[Cul-5]].add(Sp)
                     DelSp=Sp+1
                  while trackDel :
                     if DelSp not in VT :
                        trackDel=False
                        break
                     DelAlts=[VT[DelSp][1]]+VT[DelSp][2].split(",")
                     DelGT=VT[DelSp][Cul] if VT[DelSp][Cul] != "0" and VT[DelSp][Cul] != "." else 0
                     if DelAlts[int(DelGT)] == '-' :
                        DelEv[SrcS[Cul-5]].add(DelSp)
                     else :
                        trackDel=False
                        break
                     DelSp+=1
                  if len(DelEv[SrcS[Cul-5]]) % 3 != 0 :
                     AltAA="Fr_Del"
                  else :
                     AltAA="Del"
               else :
                  AltCdn=''
                  for CdnP in sorted(RfCdn.keys()) :
                     if CdnP not in VT :
                        AltCdn = AltCdn+RfCdn[CdnP]
                     else :
                        AltCdn = AltCdn+GT2NT(RFseq,CdnP,VT[CdnP],Cul-4)
                  if '-' in AltCdn : continue
                  AltAA=codontable[''.join(AltCdn)]
               if VT[Sp][4] == '.' :
                  VT[Sp][4] = AltAA
               elif AltAA not in VT[Sp][4].split(",") :
                  VT[Sp][4]= VT[Sp][4]+","+AltAA

               if AltAA == 'Fr_Ins' or AltAA == 'Fr_Del' :
                  VSum[SrcS[Cul-5]]['Fr']=1
               elif AltAA == '_' and VT[Sp][3] != '_' :
                  VSum[SrcS[Cul-5]]['ErStop']=1
               elif AltAA == 'Ins' :
                  VSum[SrcS[Cul-5]]['Ins']+=1
               elif AltAA == 'Del' :
                  VSum[SrcS[Cul-5]]['Del']+=1
               elif AltAA != VT[Sp][3] :
                  VSum[SrcS[Cul-5]]['NS']+=1
               elif AltAA == VT[Sp][3] :
                  VSum[SrcS[Cul-5]]['S']+=1
               
        print "\t".join([str(Sp+1)]+[str(i) for i in Vr])

    print "\t".join(["SampID"]+sorted(VarType))
    for s,v in VSum.iteritems() :
        print "\t".join([s]+[str(v[x]) for x in sorted(v)])

def findCdnPos(RfSeq,Sp,RfSp) :
    ''' Find the reference codon ,base on the given sequence and the point
        Return a list of coordinators on the given sequence
    '''
    lenRf=len(RfSeq)
    Cd1st=Sp-(RfSp % 3)+1 if RfSp % 3 > 0 else Sp-3+1
    CdP = [ Cd1st, Cd1st+1, Cd1st+2 ]
    findFD=-1
    while RfSeq[CdP[0]] == '-' :
          CdP[0]+=findFD
          if CdP[0] < 0 :
             findFD=1
          #   print "Some thing wrong!"
          #   break
    CdP[1:]=[CdP[0]+1,CdP[0]+2]
    while RfSeq[CdP[1]] == '-' :
          CdP[1]+=1
          if CdP[2] > lenRf :
             print "Can find the end of AA!",CdP
             break
    CdP[2]=CdP[1]+1
    while RfSeq[CdP[2]] == '-' :
          CdP[2]+=1
          if CdP[2] > lenRf :
             print "The "+Cdp[2]+" to big. Can find the end of AA!",CdP
             break
    return CdP

def GT2NT(RFseq,Sp,VT,sNo) :
    Cul=sNo+4
    if VT[Cul]== '.' or VT[Cul] == '0' :
       return RFseq[Sp]
    ALTs=[VT[1]]+VT[2].split(",")
    return ALTs[int(VT[Cul])]

def trackIns(VT,sNo) :
    '''
      check if 
    '''

# Start of main scripts
OtCand=sys.argv[1]
OCFL=open("Orth_"+OtCand+".lst","r")
with open("Orth_"+OtCand+".fa","r") as OCFA :
     data = OCFA.read()
OCForf=open("Orth_"+OtCand+"_orf.fa","w")
ORFMSA="Orth_"+OtCand+"_orf_CLW.fa"
OCA={}
for rec in OCFL :
   rec=rec.rstrip("\n")
   recA=rec.split("\t")
   OCA[recA[0]]=recA

data = [ x.split('\n',1) for x in data.split('>')]
data = [(x[0],''.join(x[1].split())) for x in data if len(x) ==2 ]

for seq in data :
   CtgSrc=seq[0][:seq[0].find("_")]
   CtgNm=seq[0][seq[0].find("_")+1:]
   if CtgSrc in OCA :
     if OCA[CtgSrc][-1]=='-' :
        S=revClSeq(seq[1])
     else :
        S=seq[1]
     if OCA[CtgSrc][1]==CtgNm :
       OCA[CtgSrc].append(S)
     else :
         print "The name of contig, "+CtgNm+", couldn't be found."+OCA[CtgSrc][0]
   else :
       print "There is no aligment for : "+CtgSrc+"."

for CtgSrc,Ctg in OCA.iteritems() :
#   print "\t".join([aln,str(len(OCA[aln][-1])),OCA[aln][-1][:10],x])
#   print list(OCA[aln][-1].start())
   LongOrf=get_orfs((Ctg[0],Ctg[-1]))
   OCForf.write(">"+Ctg[0]+"_"+Ctg[1]+"\n"+LongOrf[3]+"\n")

OCFL.close()
OCFA.close()
OCForf.close()
'''
signMSA=subprocess.call(['clustalw2','-ALIGN',
          '-INFILE=Orth_'+OtCand+'_orf.fa','-TYPE=DNA',
          '-OUTFILE='+ORFMSA,'-OUTPUT=FASTA',
          '-OUTORDER=INPUT','-DNAMATRIX=IUB'])
'''
signMSA=0
if signMSA == 0 :
   scanVar(ORFMSA,'S1s')
   for s,v in VSum.iteritems() :
       print s,v.values()
else :
  print "something wrong with alignment"
