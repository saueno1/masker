#!/usr/bin/python
import sys

inBED=sys.argv[1]

tp=( "mRNA", "start_codon", "CDS", "stop_codon" )
Fsrc="predicted"


with open(inBED,"r") as inB :
    for rl in inB :
       rlA=rl.rstrip("\n").split("\t")
       GId=rlA[0]
       CDSSt=str(int(rlA[1])+1)
       CDSEd=str(int(rlA[2])-3)
       mRNAEd=rlA[3]
       print("\t".join([ GId, Fsrc, "start_codon", CDSSt, str(int(CDSSt)+2), ".", "+", "0", "transcript_id \""+GId ]))
       print("\t".join([ GId, Fsrc, "CDS", CDSSt, CDSEd, ".", "+", "0", "transcript_id \""+GId ]))
       print("\t".join([ GId, Fsrc, "stop_codon", str(int(CDSEd)+1), str(int(CDSEd)+3), ".", "+", "0", "transcript_id \""+GId ]))
     
