#!/usr/bin/python
import sys,gzip

if len(sys.argv) < 2 :
   inf = sys.stdin
elif sys.argv[1][-3:]==".gz" :
   inf =  gzip.open(sys.argv[1],"rb")


for rl in inf :
       if rl[0] == "#" : continue
       rlA=rl.rstrip("\n").split("\t")
       GId=rlA[0]
       POS=rlA[1]
       VCQ=rlA[5]
       PASS=rlA[6]
       INFO={}
       for infoI in rlA[7].split(";") :
          if "=" in infoI :
            (tag,cont)=infoI.split("=")
          else :
            tag=infoI
            cont=True
          INFO[tag]=cont
#       INFO={ k:v for k,v in (x.split("=") for x in rlA[7].split(";")) }
       ANN=[ x.split("|") for x in INFO["ANN"].split(",") ]
       if 'INDEL' in INFO :
         for Eff in ANN :
#          if "CDS_EST" in Eff[3] :
#          if "upstream" not in Eff[1] and "downstream" not in Eff[1] :
             print("\t".join([GId,POS,VCQ,PASS,Eff[1],Eff[2],Eff[3]]))
