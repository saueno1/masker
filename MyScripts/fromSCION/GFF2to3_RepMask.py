#!/usr/bin/python
import sys,gzip

with open(sys.argv[1],"r") as GF2 :
   for gf2l in GF2 :
      gf2l=gf2l.rstrip("\n")
      gf2A=gf2l.split("\t")
      if len(gf2A) < 9 :
         continue
      TgA=gf2A[8].split(" ")
      gf2A[2]="dispersed_repeat"
      attr="Target="+" ".join([TgA[1][1:-1],TgA[2],TgA[3],gf2A[6]])
      print "\t".join(gf2A[:-1]+[attr])
     
