#!/usr/bin/python
from Bio import SeqIO
import sys

with open(sys.argv[1],"rU") as handle:
  for record in SeqIO.parse(handle, "fasta"):
      print(record.id+"\t"+str(len(record.seq)))
