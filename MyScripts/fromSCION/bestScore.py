#!/usr/bin/python
'''
Author	:	Fu-Jin Wei
Date	:	Sep. 14, 2017 ( original Sep. 7, 2017)
command	:	bestScore.py RefGene
Require :	The FASTA file for multiple alignment to produce score_ascii file before.
Descript	:
  Join with the result of multiple alignment and contig sequence file,
this script would make a FASTA file contain high-score sequences only.
  Two functions within, bestSC() and retrSeq() . bestSc() is to fetch the 
score table and return the sequence id list from high score to low.
retrSeq() is according the giving list to retrive sequence from FASTA file.
'''

import sys

def bestSc( SAfn ) :
  CtgSc={}
  ScCons=0
  HLCtg=[]
  with open(SAfn, "r") as SA :
    inTab=0
    for sl in SA :
       if inTab==1 :
          ( Ctg,d,Sc ) = sl.rstrip("\n").split()
          CtgSc[Ctg]=int(Sc)
       if sl[:13] == " BAD AVG GOOD" :
          next(SA)
          inTab=1
       if sl[:4] == "cons" :
          break
  ScCons=CtgSc["cons"]
  for Ctg,Sc in sorted(CtgSc.iteritems(), key=lambda (k,v): (v,k), reverse=True ) :
    if Sc >= ScCons and Ctg != "cons" :
#       print "\t".join([Ctg,":",str(Sc)])
       HLCtg.append(Ctg)
    else :
       CtgSc.pop(Ctg,None)
  return HLCtg

def retrSeq( FAfn, SeqIDs, outFA ):
  FAs={}
  with open(FAfn, 'r') as f :
      FAs = f.read()
  FAs = [ x.split("\n",1) for x in FAs.split('>')]
  FAs = { x[0].split()[0] : ''.join(x[1].split()).rstrip("\n") for x in FAs if len(x)==2 }
  if outFA == '' :
     outFA=FAfn[:FAfn.find(".fa")]+"_sel.fa"
  with open(outFA,"w") as wFA :
     for ks in SeqIDs :
        if ks not in FAs :
          print ks+" not in FASTA file, "+FAfn
        wFA.write(">"+ks+"\n"+FAs[ks]+"\n")

g=sys.argv[1]
if sys.argv[1].find(".") != -1 :
  SAfn=sys.argv[1][:sys.argv[1].find(".")]+".score_ascii"
else :
  SAfn=sys.argv[1]+".score_ascii"

CtgFAfn=g+"_OrthCtg.fa"
HmCtgfn=g+"_OrthCtg_Hm.fa"

Ctgs=bestSc(SAfn)
retrSeq(CtgFAfn, Ctgs, HmCtgfn)
