#!/usr/bin/python
# read the table for ORF in contigs and GFF by InterProScan for longest protein of contigs
# translate the coordinates to based on contigs

import sys

if len(sys.argv) < 3 :
   print "Need two files"
   exit()

OiCA={}
with open(sys.argv[1]) as OiC :
  for l in OiC :
     l=l.rstrip("\n")
     lA=l.split()
     OiCA[lA[0]]=lA[1:]

with open(sys.argv[2]) as GF :
  for l in GF :
     l=l.rstrip("\n")
     if l[:5]=="##seq" :
       lA=l.split()
       print " ".join(lA[:2]+["1",OiCA[lA[1]][3]])
     elif l[:7]=="##FASTA" :
       break
     elif l[:2]=="##" :
       print l
       continue
     else :
       lA = l.split("\t")
       SId = lA[0]
       OStC = OiCA[lA[0]][0]
       OEdC = OiCA[lA[0]][1]
       lA[8]=lA[8].replace("ID=","ID=pep_")
       lA[8]=lA[8].replace("Target=","Target=pep_")
       if lA[2] == "polypeptide" :
          lA[1] = "getorf"
          print "\t".join([SId,lA[1],"ORF",OStC,OEdC,".",OiCA[lA[0]][2],".","Target=pep_"+SId+" "+lA[3]+" "+lA[4]+";ID=orf_"+SId+"_"+OStC+"_"+OEdC])
       print "\t".join(lA)

OiC.close()
GF.close()
