#!/usr/bin/python
# usage : IdMYB.py <MYB list> <gzip, 4 columns file for the ratio of Query/Subject> <gzip, EST blastagainst genomic sequences in outfmt #6 >
import sys, gzip

RaQS=0.8
MYBlist={}

with open(sys.argv[1], 'r') as mf :
    for l in mf :
       MYBlist[l.rstrip()]=[]

with gzip.open(sys.argv[2],'rb') as RQf :
    for l in RQf :
       RA=l.rstrip().split("\t")
       if RA[0] in MYBlist and float(RA[3]) >= RaQS :
          MYBlist[RA[0]].append(RA[1])

with gzip.open(sys.argv[3],'rb') as compR :
    for l in compR :
       l=l.rstrip()
       RA=l.split("\t")
       if RA[0] in MYBlist and RA[1] in MYBlist[RA[0]] :
          print(l)
