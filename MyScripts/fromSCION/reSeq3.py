#!/bin/python
# import FASTA file from 1st argument or stdin
# Output sequence ID, length . And calculate N50, sum of length and sequencen number.
import sys,re

if len(sys.argv) > 1 :
  inFa=open(sys.argv[1],"r")
else :
  inFa=sys.stdin
lSeq=[ "NullSeq", ""]
SeqLen={}
for FaLine in inFa :
  FaLine=FaLine.rstrip("\n")
  if FaLine[0] == ">" :
     if lSeq[0] != "NullSeq" :
        SeqLen[lSeq[0]]=len(lSeq[1])
        print("\t".join([lSeq[0],str(len(lSeq[1]))]))
        lSeq[1]=""
     lSeq[0]=FaLine[1:].split()[0]
  else :
     lSeq[1]+=re.sub(r'[^ATGCUatgcu]','',FaLine)
SeqLen[lSeq[0]]=len(lSeq[1])
sumLen=sum(SeqLen.values())
print("\t".join([lSeq[0],str(len(lSeq[1]))]))
vSum=0
for i in sorted(SeqLen.iteritems(), key=lambda x: x[1], reverse=True) :
   vSum+=i[1]
   if float(vSum)/float(sumLen) > 0.5 :
      print('\t'.join(["N50 :",str(i[1]),
                       ", sum of lengh :",str(sumLen),
                       ", total sequences number :",str(len(SeqLen))
                      ]))
      break
