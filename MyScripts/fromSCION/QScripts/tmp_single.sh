#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o qSingle_S3sUS4s-S8HK5.out 
#PBS -e qSingle_S3sUS4s-S8HK5.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N mpS3sUS4sS8HK5
cd /lfs/maskerwei/Trinity_out_S8HK5/fromS8HK5toSxx
samtools mpileup -l S3sUS4s-S8HK5.bed.gz \
      --reference S8HK5.fa -b S8HK5BAM.lst \
  | bgzip -c > S3sUS4s-S8HK5.mpileup.txt.gz &
