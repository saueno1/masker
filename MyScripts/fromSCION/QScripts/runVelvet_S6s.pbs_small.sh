#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S6s.out 
#PBS -e qSmall_Velvet_S6s.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S6s
cd /lfs/maskerwei/Velvet
#velveth S6s 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#	/lfs/maskerwei/FASTQs/Ir3639_R1.fq.gz   /lfs/maskerwei/FASTQs/Ir3639_R2.fq.gz \
#    -shortPaired2 \
#	/lfs/maskerwei/FASTQs/Ir3640_R1.fq.gz   /lfs/maskerwei/FASTQs/Ir3640_R2.fq.gz \
#    -shortPaired3 \
#	/lfs/maskerwei/FASTQs/MiSeq_S6_R1.fq.gz /lfs/maskerwei/FASTQs/MiSeq_S6_R2.fq.gz
#for ((i=31;i<45;i+=4))
#do
#  velvetg S6s_${i} -read_trkg yes
#  oases S6s_${i} -min_pair_count 10 -min_trans_lgth 300
#done
velveth Merged_S6s 29 -long S6s_*/transcripts.fa &> LOGs/velveth_merged_S6s.log
velvetg Merged_S6s -read_trkg yes -conserveLong yes &> LOGs/velvetg_merged_S6s.log && oases Merged_S6s -merge yes &> LOGs/oases_merged_S6s.log
