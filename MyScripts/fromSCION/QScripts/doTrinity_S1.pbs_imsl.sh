#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_S1.out 
#PBS -e qImsl_S1.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS1 
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --CPU 28 --max_memory 100G --seqType fq \
  --samples_file grp_S1_FQs.txt \
  --long_reads /lfs/maskerwei/PacBio/SR0004_S1.fa \
  --min_contig_length 500 \
  --output /lfs/maskerwei/trinity_out_S1s &>> trinity_S1s_pbs.log
#cat /lfs/maskerwei/trinity_out_MiSeq_S1/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S1.fa.bz2
