#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_Ooi7.out 
#PBS -e qImsl_Ooi7.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinOoi7 
cd /home/maskerwei/RNA-seq/ms1-ms4
Trinity --CPU 28 --max_memory 110G --seqType fq \
  --left /lfs/maskerwei/FASTQs/MiSeq_S5_R1.fq.gz \
  --right /lfs/maskerwei/FASTQs/MiSeq_S5_R2.fq.gz \
  --min_contig_length 500 \
  --output /lfs/maskerwei/trinity_out_S5s &>> trinity_S5s_pbs.log
#cat /lfs/maskerwei/trinity_out_MiSeq_S2/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S2.fa.bz2
