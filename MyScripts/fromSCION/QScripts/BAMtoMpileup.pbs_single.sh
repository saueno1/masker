#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o qSingle_QLib-DBLib.out 
#PBS -e qSingle_QLib-DBLib.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N mpQLibDBLib
cd /lfs/maskerwei/Trinity_out_DBLib/fromDBLibtoSxx
samtools mpileup -l QLib-DBLib.bed.gz \
      --reference DBLib.fa -b DBLibBAM.lst \
  | bgzip -c > QLib-DBLib.mpileup.txt.gz &
