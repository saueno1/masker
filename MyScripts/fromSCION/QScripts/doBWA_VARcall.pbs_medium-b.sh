#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_BWA_170.out 
#PBS -e qMedB_BWA_170.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BWA170
cd /lfs/maskerwei/Sugi_genome/Sequence_Data/
./doBWA_VarCall_64c.bsh Ip2189_170bp_cut Kuni3_S3-3 
