#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=14
#PBS -l memsz_job=30GB 
#PBS -o qSmall_BLASTP-NR_QFA.out 
#PBS -e qSmall_BLASTP-NR_QFA.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BLNRQFA
cd /lfs/maskerwei/EviGene/okayset
blastp -query QFA -db refseq_protein \
  -task blastp -num_threads 14 \
  -outfmt "6 std qlen slen stitle staxid sskingdom" \
  -max_target_seqs 2 -out QFA-refseq_protein.outfmt6.txt
