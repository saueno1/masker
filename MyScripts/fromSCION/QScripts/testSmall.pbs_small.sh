#!/bin/bash
#PBS -q small 
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o standard.out 
#PBS -e standard.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N testSm
echo $HOSTNAME
head /proc/cpuinfo
