#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=20GB 
#PBS -o qSmall_snpEff_DBLib.out 
#PBS -e qSmall_snpEff_DBLib.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N SNPEFF_DBLib
cd /lfs/maskerwei/pickESTs/TenBAMs
java -jar /lfs/maskerwei/snpEff/snpEff.jar eff \
     -csvStats var_DBLib.genes_snpEff.txt \
     -s var_DBLib.summary.html sugiEST3001 var_DBLib-sugiEST3001.flt.vcf.gz | bgzip -c > var_DBLib-sugiEST3001.snpEff.vcf.gz
