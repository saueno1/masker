#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_RepMsk_DBLib.out 
#PBS -e qSmall_RepMak_DBLib.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RM_DBLib
cd /lfs/maskerwei/trinity_out_DBLib
runRepeatMasker.bsh Trinity_DBLib 
