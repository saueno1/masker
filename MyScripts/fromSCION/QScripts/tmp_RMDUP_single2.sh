#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o Single_RMDUP_S8HK5-S8HK5.out 
#PBS -e Single_RMDUP_S8HK5-S8HK5.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RMDUPS8HK5S8HK5
cd /lfs/maskerwei/Trinity_out_S8HK5
mv Trinity_S8HK5-S8HK5_NI.bam S8HK5-S8HK5_NI.ud.bam && samtools rmdup --reference S8HK5.fa S8HK5-S8HK5_NI.ud.bam S8HK5-S8HK5_NI.bam
samtools index S8HK5-S8HK5_NI.bam
