#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o Single_IsecAll_DBLib.out 
#PBS -e Single_IsecAll_DBLib.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N IsecDBLib
cd /lfs/maskerwei/Trinity_out_DBLib/fromDBLibtoSxx
VCFtoMpileup.bsh "LibList" DBLib &> LOGs/VCFtoMpileup.log
VCFtoMpileup2.bsh "LibList" DBLib &>> LOGs/VCFtoMpileup.log
VCFtoMpileup3.bsh "LibList" DBLib &>> LOGs/VCFtoMpileup.log
