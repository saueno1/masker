#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=25
#PBS -l memsz_job=100GB 
#PBS -o qImsl_S3.out 
#PBS -e qImsl_S3.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinitS3 
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --CPU 25 --max_memory 100G --seqType fq \
  --left /lfs/maskerwei/FASTQs/MiSeq_S3_R1.fq.gz \
  --right /lfs/maskerwei/FASTQs/MiSeq_S3_R2.fq.gz \
  --long_reads /lfs/maskerwei/PacBio/SR0004_S3.fa \
  --min_contig_length 500 \
  --output /lfs/maskerwei/trinity_out_S3s &>> trinity_S3s_pbs.log
#cat /lfs/maskerwei/trinity_out_MiSeq_S3/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S3.fa.bz2
