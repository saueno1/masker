#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RefGene.out 
#PBS -e qImsl_RefGene.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BUS_DBLib
cd /lfs/maskerwei/Trinity_out_DBLib
~/.local/BUSCO/scripts/run_BUSCO.py -m transcriptome \
  -i Trinity_DBLib.fa -o Trinity_DBLib_BUSCO \
  -l /lfs/maskerwei/sequences/embryophyta_odb9 \
  -c 28 -e 1e-03 -t /lfs/maskerwei/tmp/ 2> runBUSCO_DBLib.err | tee runBUSCO_DBLib.log
cd -
