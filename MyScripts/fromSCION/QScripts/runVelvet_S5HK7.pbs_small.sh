#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S5HK7.out 
#PBS -e qSmall_Velvet_S5HK7.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S5HK7
cd /lfs/maskerwei/Velvet
#velveth S5HK7 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4040_R1.fq.gz \
#        /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4040_R2.fq.gz
#for ((i=31;i<45;i+=4))
#do
#  velvetg S5HK7_${i} -read_trkg yes
#  oases S5HK7_${i} -min_pair_count 10 -min_trans_lgth 300
#done
velveth Merged_S5HK7 29 -long S5HK7_*/transcripts.fa &> LOGs/velveth_merged_S5HK7.log
velvetg Merged_S5HK7 -read_trkg yes -conserveLong yes &> LOGs/velvetg_merged_S5HK7.log && oases Merged_S5HK7 -merge yes &> LOGs/oases_merged_S5HK7.log
