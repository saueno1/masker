#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_Ooi7.out 
#PBS -e qSmall_Velvet_Ooi7.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_Ooi7
cd /lfs/maskerwei/Velvet
#velveth Ooi7 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#        /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R1.fq.gz \
#        /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R2.fq.gz
#for ((i=31;i<45;i+=4))
#do
#  velvetg Ooi7_${i} -read_trkg yes
#  oases Ooi7_${i} -min_pair_count 10 -min_trans_lgth 300
#done
velveth Merged_Ooi7 29 -long Ooi7_*/transcripts.fa &> LOGs/velveth_merged_Ooi7.log
velvetg Merged_Ooi7 -read_trkg yes -conserveLong yes &> LOGs/velvetg_merged_Ooi7.log && oases Merged_Ooi7 -merge yes &> LOGs/oases_merged_Ooi7.log
