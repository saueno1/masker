#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_S3s.out 
#PBS -e qMedB_S3s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS3s 
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --CPU 64 --max_memory 200G --seqType fq \
  --samples_file grp_S3_FQs.txt \
  --min_contig_length 500 \
  --output /lfs/maskerwei/Trinity_out_S3s &>> Trinity_S3s_pbs.log
mkdir /lfs/maskerwei/RSEM2_Trinity_S3s
cd /lfs/maskerwei/RSEM2_Trinity_S3s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../Trinity_out_S3s/Trinity.fasta > Trinity_S3s.gene_trans_map 2> LOGs/Trans_map_S3s.log
bowtie2-build --threads 64 ../Trinity_out_S3s/Trinity.fasta Trinity_S3s &> LOGs/Bowtie2_build_S3s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S3s.gene_trans_map ../Trinity_out_S3s/Trinity.fasta Trinity_S3s &> LOGs/RSEM2_Pre_Ref_S3s.log
rsem-calculate-expression -p 64 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R1.fq.gz \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R2.fq.gz \
	Trinity_S3s RSEM2_S3s &> LOGs/RSEM2_Cal_Exp_S3s.log
