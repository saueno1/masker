#!/bin/bash
#PBS -q large
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=20 
#PBS -l memsz_job=60GB 
#PBS -o standard.out 
#PBS -e standard.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N testLg
echo $HOSTNAME
head /proc/cpuinfo
