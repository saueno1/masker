#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_kallisto_DBLib.out 
#PBS -e qSmall_kallisto_DBLib.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N DBLib_kallisto
cd /lfs/maskerwei/pickESTs/byKallisto
$HOME/bin/trinityrnaseq-Trinity-v2.4.0/util/align_and_estimate_abundance.pl --seqType fq \
        --thread_count 28 \
        --samples_file grp_DBLib_FQs.txt \
	--transcripts sugiEST2910.fa \
        --output_prefix  DBLib-sugiEST2910 \
        --est_method kallisto --kallisto_add_opts "-b 100 -t 28" \
        --aln_method bowtie2 --coordsort_bam \
        --output_dir DBLib-sugiEST2910_kallisto &> LOGs/kallisto_Cal_Exp_DBLib.log
