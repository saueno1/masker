#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S8HK5.out 
#PBS -e qSmall_Velvet_S8HK5.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S8HK5
cd /lfs/maskerwei/Velvet
#velveth S8HK5 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4041_R1.fq.gz \
#        /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4041_R2.fq.gz
#for ((i=31;i<45;i+=4))
#do
#  velvetg S8HK5_${i} -read_trkg yes
#  oases S8HK5_${i} -min_pair_count 10 -min_trans_lgth 300
#done
velveth Merged_S8HK5 29 -long S8HK5_*/transcripts.fa &> LOGs/velveth_merged_S8HK5.log
velvetg Merged_S8HK5 -read_trkg yes -conserveLong yes &> LOGs/velvetg_merged_S8HK5.log && oases Merged_S8HK5 -merge yes &> LOGs/oases_merged_S8HK5.log
