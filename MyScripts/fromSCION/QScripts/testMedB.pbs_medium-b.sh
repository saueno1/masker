#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o testMedB.out 
#PBS -e testMedB.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N testMedB
echo $HOSTNAME
head /proc/cpuinfo
