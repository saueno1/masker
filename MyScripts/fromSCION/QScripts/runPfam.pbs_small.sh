#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_PFamscan_InFA.out 
#PBS -e qSmall_PFamscan_InFA.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N PFInFA
cd /lfs/maskerwei/EviGene/MYBs/splitFAs
pfam_scan.pl -fasta InFA -outfile InFA.pfam -dir /lfs/maskerwei/sequences
