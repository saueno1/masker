#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S1s.out 
#PBS -e qSmall_Velvet_S1s.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S1s
cd /lfs/maskerwei/Velvet
#velveth S1s 31,45,4 -create_binary -separate -fastq.gz -shortPaired \
#    /lfs/maskerwei/FASTQs/Ir3633_R1.fq.gz /lfs/maskerwei/FASTQs/Ir3633_R2.fq.gz \
#    -shortPaired2 \
#    /lfs/maskerwei/FASTQs/Ir3634_R1.fq.gz /lfs/maskerwei/FASTQs/Ir3634_R2.fq.gz \
#    -shortParied3 \
#    /lfs/maskerwei/FASTQs/Ir3635_R1.fq.gz /lfs/maskerwei/FASTQs/Ir3635_R2.fq.gz \
#    -shortPaired4 /lfs/maskerwei/FASTQs/MiSeq_S1_R1.fq.gz /lfs/maskerwei/FASTQs/MiSeq_S1_R2.fq.gz
velvetg S1s_31 -read_trkg yes
oases S1s_31 -min_pair_count 10 -min_trans_lgth 300
velvetg S1s_35 -read_trkg yes
oases S1s_35 -min_pair_count 10 -min_trans_lgth 300
velvetg S1s_39 -read_trkg yes
oases S1s_39 -min_pair_count 10 -min_trans_lgth 300
velvetg S1s_43 -read_trkg yes
oases S1s_43 -min_pair_count 10 -min_trans_lgth 300
