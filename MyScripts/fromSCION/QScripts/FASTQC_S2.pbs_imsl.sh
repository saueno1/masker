#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=20
#PBS -l memsz_job=60GB 
#PBS -o qImsl_S2.out 
#PBS -e qImsl_S2.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N FASTQC_S2 
mkdir /lfs/maskerwei/FASTQC
cd /home/maskerwei/RNA-seq/ms1/PC
fastqc -t 20 -o /lfs/maskerwei/FASTQC -f fastq cutAdapt_MiSeq/FFPRI-cDNA-02_S2_L001_R*.fq.gz
cp -rp /lfs/maskerwei/FASTQC/* $HOME/RNA-seq/ms1/PC/cutAdapt_MiSeq/FASTQC/
