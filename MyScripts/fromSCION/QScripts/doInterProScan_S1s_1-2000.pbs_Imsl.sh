#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qMedB_S1s_1-2000.out 
#PBS -e qMedB_S1s_1-2000.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N IPSo1-2000
cd /lfs/maskerwei/Trinity_out_S1s/Trinity_S1s_AAFAs
bzcat Trinity_S1s_orfAA_1-2000.fa.bz2 > Trinity_S1s_orfAA_1-2000.fa
/lfs/maskerwei/InterProScan/interproscan.sh -i Trinity_S1s_orfAA_1-2000.fa \
      -t p -T /lfs/maskerwei/tmp -f gff3,tsv,json -b IPS/Trinity_S1s_orfAA_1-2000 \
      -pa -iprlookup -goterms \
      -dp --cpu 28 &> IPS/IPS_S1s_orfAA_1-2000.log
