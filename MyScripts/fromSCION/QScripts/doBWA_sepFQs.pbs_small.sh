#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_BWA_FQID-RFID.out 
#PBS -e qSmall_BWA_FQID-RFID.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BwFQIDRFID
cd /lfs/maskerwei/EviGene/sepBAMs
doBWA_28c_sepFQ.bsh SM FQID RFID
