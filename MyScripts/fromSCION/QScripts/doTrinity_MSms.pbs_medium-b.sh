#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=40
#PBS -l memsz_job=200GB 
#PBS -o qMedB_MS1ms1.out 
#PBS -e qMedB_MS1ms1.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinitMs
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --seqType fq --CPU 40 --max_memory 200G \
  --left /lfs/maskerwei/FASTQs/MiSeq_S3_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir713_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R1.fq.gz \
  --right /lfs/maskerwei/FASTQs/MiSeq_S3_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir713_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R2.fq.gz \
  --long_reads /lfs/maskerwei/PacBio/SR0004_S3.fa \
  --min_contig_length 500 --output /lfs/maskerwei/trinity_out_MS1ms1 &> trinity_MSms_pbs.log
# cat /lfs/maskerwei/trinity_out_ms1/Trinity.fasta | bzip2 -c9 > Trinity_out_ms1.fa.bz2
