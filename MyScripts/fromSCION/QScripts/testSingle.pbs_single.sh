#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o testSingle.out 
#PBS -e testSingle.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N testSingle
echo $HOSTNAME
head /proc/cpuinfo
