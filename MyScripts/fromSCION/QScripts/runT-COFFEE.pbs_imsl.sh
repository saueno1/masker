#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_AT4G00720.1.out 
#PBS -e qImsl_AT4G00720.1.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Tcf_AT4G00720.1
cd /lfs/maskerwei/crossLibs/OrthCtgs
t_coffee -in=AT4G00720.1_OrthCtg.fa -mode=procoffee \
  -method=t_coffee_msa -output=clustalw_aln score_ascii \
  -maxnseq=150 -maxlen=10000 -case=upper -seqnos=off \
  -outorder=input -run_name=AT4G00720.1 -multi_core=2 \
  -quiet=AT4G00720.1_MSA.log
