#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_MYB_APBOCac_b500.out 
#PBS -e qSmall_MYB_APBOCac_b500.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N MLb5APBOCac
cd /lfs/maskerwei/pickESTs/toOrthCand/orthologs/MYB/T-COFFEE
mpirun -np 28 phyml -i MYB_APBOCac.phylip \
       -d aa -b 500 -m LG -s SPR --run_id ${PBS_JOBID%.*} \
       --rand_start --no_memory_check &> LOGs/runPhyML_MYB_APBOCac_b500.log 
