#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=10
#PBS -l memsz_job=110GB 
#PBS -o qImsl_S2.out 
#PBS -e qImsl_S2.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinitS2 
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --CPU 10 --max_memory 110G --seqType fq \
  --samples_file grp_S2_FQs.txt \
  --long_reads /lfs/maskerwei/PacBio/SR0004_S2.fa \
  --min_contig_length 500 \
  --output /lfs/maskerwei/trinity_out_S2s &>> trinity_S2s_pbs.log
#cat /lfs/maskerwei/trinity_out_MiSeq_S2/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S2.fa.bz2
