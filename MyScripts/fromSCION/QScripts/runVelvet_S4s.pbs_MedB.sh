#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_Velvet_S4s.out 
#PBS -e qMedB_Velvet_S4s.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S4s
cd /lfs/maskerwei/Velvet
#velveth S4s 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#	/lfs/maskerwei/FASTQs/HiSeq_Ir718_R1.fq.gz      /lfs/maskerwei/FASTQs/HiSeq_Ir718_R2.fq.gz \
#    -shortPaired2 \
#	/lfs/maskerwei/FASTQs/HiSeq_Ir719_R1.fq.gz      /lfs/maskerwei/FASTQs/HiSeq_Ir719_R2.fq.gz \
#    -shortPaired3 \
#	/lfs/maskerwei/FASTQs/HiSeq_Ir720_R1.fq.gz      /lfs/maskerwei/FASTQs/HiSeq_Ir720_R2.fq.gz \
#    -shortPaired4 \
#	/lfs/maskerwei/FASTQs/HiSeq_Ir721_R1.fq.gz      /lfs/maskerwei/FASTQs/HiSeq_Ir721_R2.fq.gz \
#    -shortPaired5 \
#	/lfs/maskerwei/FASTQs/HiSeq_Ir722_R1.fq.gz      /lfs/maskerwei/FASTQs/HiSeq_Ir722_R2.fq.gz \
#    -shortPaired6 \
#	/lfs/maskerwei/FASTQs/MiSeq_S4_R1.fq.gz /lfs/maskerwei/FASTQs/MiSeq_S4_R2.fq.gz
velvetg S4s_31 -read_trkg yes
oases S4s_31 -min_pair_count 10 -min_trans_lgth 300
velvetg S4s_35 -read_trkg yes
oases S4s_35 -min_pair_count 10 -min_trans_lgth 300
velvetg S4s_39 -read_trkg yes
oases S4s_39 -min_pair_count 10 -min_trans_lgth 300
velvetg S4s_43 -read_trkg yes
oases S4s_43 -min_pair_count 10 -min_trans_lgth 300
