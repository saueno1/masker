#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_S1s.out 
#PBS -e qMedB_S1s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N IPS_S1s
cd /lfs/maskerwei/Trinity_out_S1s
/lfs/maskerwei/InterProScan/interproscan.sh -i Trinity.fasta \
      -t n -T /lfs/maskerwei/tmp -f gff3,tsv,json -b IPS/Trinity_S1s \
      -pa -iprlookup -goterms \
      -dp --cpu 64 &> IPS/IPS.log
