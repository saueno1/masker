#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=20GB 
#PBS -o qSmall_BLASTP-Abies-OrthCand_Arab.out 
#PBS -e qSmall_BLASTP-Abies-OrthCand_Arab.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BLT_Abies-Arab
cd /lfs/maskerwei/compare/Abies
blastp -query <(zcat Abies.pep.fa.gz ) -db OrthCand_Arab.pep \
       -outfmt "6 std qlen slen" -out Abies-OrthCand_Arab.outfmt6.txt \
       -max_target_seqs 1 \
       -num_threads 28
