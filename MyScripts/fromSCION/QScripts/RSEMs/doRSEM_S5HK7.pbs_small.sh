#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qSmall_RSEM_S5HK7.out 
#PBS -e qSmall_RSEM_S5HK7.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMS5HK7
cd /lfs/maskerwei/RSEM_Trinity_S5HK7
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S5HK7/Trinity.fasta > Trinity_S5HK7.gene_trans_map 2> LOGs/trans_map_S5HK7.log
bowtie2-build --threads 28 ../trinity_out_S5HK7/Trinity.fasta Trinity_S5HK7 &> LOGs/bowtie2-build_S5HK7.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S5HK7.gene_trans_map ../trinity_out_S5HK7/Trinity.fasta Trinity_S5HK7 &> LOGs/RSEM_prep_ref_S5HK7.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir4040_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir4040_R2.fq.gz \
	Trinity_S5HK7 RSEM_S5HK7 &> LOGs/RSEM_Cal_Exp_S5HK7.log
