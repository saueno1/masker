#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qSmall_RSEM_S2s.out 
#PBS -e qSmall_RSEM_S2s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S2s 
cd /lfs/maskerwei/RSEM_Trinity_S2s
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S2s/Trinity.fasta > Trinity_S2s.gene_trans_map
bowtie2-build --threads 28 ../trinity_out_S2s/Trinity.fasta Trinity_S2s
rsem-prepare-reference  --transcript-to-gene-map Trinity_S2s.gene_trans_map ../trinity_out_S2s/Trinity.fasta Trinity_S2s
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3636_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3636_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R2.fq.gz \
	Trinity_S2s RSEM_S2s
