#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_RSEM_S1NK4.out 
#PBS -e qImsl_RSEM_S1NK4.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMS1NK4
cd /lfs/maskerwei/RSEM_Trinity_S1NK4
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S1NK4/Trinity.fasta > Trinity_S1NK4.gene_trans_map 2> LOGs/trans_map_S1NK4.log
bowtie2-build --threads 28 ../trinity_out_S1NK4/Trinity.fasta Trinity_S1NK4 &> LOGs/bowtie2-build_S1NK4.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S1NK4.gene_trans_map ../trinity_out_S1NK4/Trinity.fasta Trinity_S1NK4 &> LOGs/RSEM_prep_ref_S1NK4.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R1.fq.gz \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R2.fq.gz \
	Trinity_S1NK4 RSEM_S1NK4 &> LOGs/RSEM_Cal_Exp_S1NK4.log
