#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_RSEM_S2s.out 
#PBS -e qImsl_RSEM_S2s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S2s 
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3636_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3636_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R2.fq.gz \
	sugiEST2910 RSEM_S2s &> LOGs/RSEM_Cal_Exp_S2s.log
