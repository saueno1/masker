#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_S1NK4.out 
#PBS -e qImsl_RSEM_S1NK4.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMS1NK4
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R1.fq.gz \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R2.fq.gz \
	sugiEST2910 RSEM_S1NK4 &> LOGs/RSEM_Cal_Exp_S1NK4.log
