#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_S3s.out 
#PBS -e qImsl_RSEM_S3s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S3s 
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R1.fq.gz \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R2.fq.gz \
	sugiEST2910 RSEM_S3s &> LOGs/RSEM_Cal_Exp_S3s.log
