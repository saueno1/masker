#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_S8HK5.out 
#PBS -e qImsl_RSEM_S8HK5.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMS8HK5
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir4041_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir4041_R2.fq.gz \
	sugiEST2910 RSEM_S8HK5 &> LOGs/RSEM_Cal_Exp_S8HK5.log
