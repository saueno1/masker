#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_RSEM_S1s.out 
#PBS -e qImsl_RSEM_S1s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S1s 
cd /lfs/maskerwei/RSEM_Trinity_S1s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S1s/Trinity.fasta > Trinity_S1s.gene_trans_map 2> LOGs/trans_map_S1s.log
bowtie2-build --threads 28 ../trinity_out_S1s/Trinity.fasta Trinity_S1s &> LOGs/bowtie2-build_S1s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S1s.gene_trans_map ../trinity_out_S1s/Trinity.fasta Trinity_S1s &> LOGs/RSEM_prep_ref_S1s.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3633_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3633_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R2.fq.gz \
	Trinity_S1s RSEM_S1s &> LOGs/RSEM_Cal_Exp_S1s.log
