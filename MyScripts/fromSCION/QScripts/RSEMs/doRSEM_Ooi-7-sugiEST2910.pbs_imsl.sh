#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_Ooi-7.out 
#PBS -e qImsl_RSEM_Ooi-7.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMOoi-7
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R1.fq.gz \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R2.fq.gz \
	sugiEST2910 RSEM_Ooi-7 &> LOGs/RSEM_Cal_Exp_Ooi-7.log
