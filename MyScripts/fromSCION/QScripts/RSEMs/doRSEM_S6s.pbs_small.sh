#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qSmall_RSEM_S6s.out 
#PBS -e qSmall_RSEM_S6s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S6s 
cd /lfs/maskerwei/RSEM_Trinity_S6s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S6s/Trinity.fasta > Trinity_S6s.gene_trans_map 2> LOGs/trans_map_S6s.log
bowtie2-build --threads 28 ../trinity_out_S6s/Trinity.fasta Trinity_S6s &> LOGs/bowtie2-build_S6s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S6s.gene_trans_map ../trinity_out_S6s/Trinity.fasta Trinity_S6s &> LOGs/RSEM_prep_ref_S6s.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3639_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3640_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S6_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3639_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3640_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S6_R2.fq.gz \
	Trinity_S6s RSEM_S6s &> LOGs/RSEM_Cal_Exp_S6s.log
