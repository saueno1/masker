#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_Ooi-7.out 
#PBS -e qImsl_RSEM_Ooi-7.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEMOoi-7
cd /lfs/maskerwei/RSEM_Trinity_Ooi-7
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_Ooi-7/Trinity.fasta > Trinity_Ooi-7.gene_trans_map 2> LOGs/trans_map_Ooi-7.log
bowtie2-build --threads 28 ../trinity_out_Ooi-7/Trinity.fasta Trinity_Ooi-7 &> LOGs/bowtie2-build_Ooi-7.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_Ooi-7.gene_trans_map ../trinity_out_Ooi-7/Trinity.fasta Trinity_Ooi-7 &> LOGs/RSEM_prep_ref_Ooi-7.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R1.fq.gz \
	/lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4038_R2.fq.gz \
	Trinity_Ooi-7 RSEM_Ooi-7 &> LOGs/RSEM_Cal_Exp_Ooi-7.log
