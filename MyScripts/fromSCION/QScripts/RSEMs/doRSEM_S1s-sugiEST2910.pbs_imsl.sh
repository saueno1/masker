#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_RSEM_S1s.out 
#PBS -e qImsl_RSEM_S1s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S1s 
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3633_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3633_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R2.fq.gz \
        sugiEST2910 RSEM_S1s &> LOGs/RSEM_Cal_Exp_S1s.log
