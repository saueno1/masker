#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSml_RSEM_S3s.out 
#PBS -e qSml_RSEM_S3s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S3s 
cd /lfs/maskerwei/RSEM_Trinity_S3s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../trinity_out_S3s/Trinity.fasta > Trinity_S3s.gene_trans_map 2> LOGs/Trans_map_S3s.log
bowtie2-build --threads 28 ../trinity_out_S3s/Trinity.fasta Trinity_S3s &> LOGs/Bowtie2_build_S3s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S3s.gene_trans_map ../trinity_out_S3s/Trinity.fasta Trinity_S3s &> LOGs/RSEM_Pre_Ref_S3s.log
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R1.fq.gz \
	/lfs/maskerwei/FASTQs/HiSeq_Ir713_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir714_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir715_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir716_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir717_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S3_R2.fq.gz \
	Trinity_S3s RSEM_S3s &> LOGs/RSEM_Cal_Exp_S3s.log
