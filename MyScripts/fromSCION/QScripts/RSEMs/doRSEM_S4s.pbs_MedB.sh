#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=60
#PBS -l memsz_job=200GB 
#PBS -o qMedB_RSEM_S4s.out 
#PBS -e qMedB_RSEM_S4s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S4s 
cd /lfs/maskerwei/RSEM_Trinity_S4s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl Trinity_S4s.fa > Trinity_S4s.gene_trans_map 2> LOGs/trans_map_S4s.log
bowtie2-build --threads 60 Trinity_S4s.fa Trinity_S4s &> LOGs/bowtie2-build_S4s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S4s.gene_trans_map Trinity_S4s.fa Trinity_S4s &> LOGs/RSEM_prep_ref_S4s.log
rsem-calculate-expression -p 60 --bowtie2 --paired-end \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R1.fq.gz \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R2.fq.gz \
    /lfs/maskerwei/RSEM_Trinity_S4s/Trinity_S4s \
    RSEM_S4s &> LOGs/RSEM_Cal_Exp_S4s.log
