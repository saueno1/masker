#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qImsl_RSEM_S4s.out 
#PBS -e qImsl_RSEM_S4s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RSEM_S4s
cd /lfs/maskerwei/pickESTs/RSEM
rsem-calculate-expression -p 28 --bowtie2 --paired-end \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R1.fq.gz \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R2.fq.gz \
    sugiEST2910 RSEM_S4s &> LOGs/RSEM_Cal_Exp_S4s.log
