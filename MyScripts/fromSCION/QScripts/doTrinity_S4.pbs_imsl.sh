#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_S4.out 
#PBS -e qImsl_S4.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Trinity_S4 
cd /home/maskerwei/RNA-seq/ms1/PC
echo ${HOSTNAME} > trinity_MiSeq_S4_pbs_imsl.log
Trinity --seqType fq --max_memory 100G --left cutAdapt_MiSeq/FFPRI-cDNA-04_S4_L001_R1.fq.gz --right cutAdapt_MiSeq/FFPRI-cDNA-04_S4_L001_R2.fq.gz --CPU 28 --min_contig_length 500 --output /lfs/maskerwei/trinity_out_MiSeq_S4 &>> trinity_MiSeq_S4_pbs.log
cat /lfs/maskerwei/trinity_out_MiSeq_S4/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S4.fa.bz2
