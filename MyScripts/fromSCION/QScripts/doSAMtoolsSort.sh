#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qMedB_sort.out 
#PBS -e qMedB_sort.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N SAMsort 
cd /lfs/maskerwei/RSEM_Trinity_ms1/
samtools sort -l 9 -@ 28 -m 3G -T RSEM_ms1.tmp \
  --reference Trinity_ms1.transcripts.fa \
  -O BAM -o RSEM_ms1.bam \
  RSEM_ms1.transcript.bam
