#!/bin/bash
#PBS -q imsl
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=100GB 
#PBS -o qImsl_S1NK4.out 
#PBS -e qImsl_S1NK4.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS1NK4 
cd /home/maskerwei/RNA-seq/ms1-ms4
Trinity --CPU 28 --max_memory 100G --seqType fq \
  --left /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R1.fq.gz \
  --right /lfs/maskerwei/FASTQs/ms1-ms4/trimmed/Ir4039_R2.fq.gz \
  --min_contig_length 500 \
  --output /lfs/maskerwei/trinity_out_S1NK4 &>> trinity_S1NK4_pbs.log
#cat /lfs/maskerwei/trinity_out_MiSeq_S2/Trinity.fasta | bzip2 -c9 > Trinity_out_Miseq_S2.fa.bz2
