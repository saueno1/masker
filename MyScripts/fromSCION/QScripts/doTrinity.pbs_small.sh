#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=20
#PBS -l memsz_job=60GB 
#PBS -o standard.out 
#PBS -e standard.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -r y
#PBS -N Trinity_S4 
$ExeH=/home/maskerwei/RNA-seq/ms1/PC
$LogF=${ExeH}/trinity_MiSeq_S4_pbs.log
$OutB=/tmp/w
$OutBF=${OutB}/trinity_out_MiSeq_S4
mkdir ${OutB}
chmod 700 ${OutB}
cd ${ExeH}
echo ${HOSTNAME} > ${LogF}
Trinity --seqType fq --max_memory 60G --left ${ExeH}/cutAdapt_MiSeq/FFPRI-cDNA-04_S4_L001_R1.fq.gz --right ${ExeH}/cutAdapt_MiSeq/FFPRI-cDNA-04_S4_L001_R2.fq.gz --CPU 20 --min_contig_length 500 --output ${OutBF} &>> ${LogF}
cd ${OutBF}
cat Trinity.fasta | bzip2 -c9 > ${ExeH}/Trinity_out_Miseq_S4.fa.bz2
