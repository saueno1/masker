#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S5s.out 
#PBS -e qSmall_Velvet_S5s.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S5s
cd /lfs/maskerwei/Velvet
#velveth S5s 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#	/lfs/maskerwei/FASTQs/MiSeq_S5_R1.fq.gz /lfs/maskerwei/FASTQs/MiSeq_S5_R2.fq.gz
#for ((i=31;i<45;i+=4))
#do
#  velvetg S5s_${i} -read_trkg yes
#  oases S5s_${i} -min_pair_count 10 -min_trans_lgth 300
#done
velveth Merged_S5s/ 35 -long S5s_*/transcripts.fa &> LOGs/velveth_merged_S5s.log
velvetg Merged_S5s/ -read_trkg yes -conserveLong yes &> LOGs/velvetg_merged_S5s.log && oases Merged_S5s/ -merge yes &> LOGs/oases_merged_S5s.log
