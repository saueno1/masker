#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=30GB 
#PBS -o qSmall_BLASTP-NR_DBLib.out 
#PBS -e qSmall_BLASTP-NR_DBLib.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BLNRDBLib
cd /lfs/maskerwei/Trinity_out_DBLib
blastp -query <(bzcat Trinity_DBLib_ORFAAs.fa.bz2) -db nr \
  -task blastp-fast -num_threads 28 \
  -outfmt "6 std qlen slen stitle staxid sskingdom" \
  -max_target_seqs 2 -out DBLib-nr.outfmt6.txt
