#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_S1s.out 
#PBS -e qMedB_S1s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS1s 
cd /home/maskerwei/RNA-seq/ms1/PC
Trinity --CPU 64 --max_memory 200G --seqType fq \
  --samples_file grp_S1_FQs.txt \
  --min_contig_length 500 \
  --output /lfs/maskerwei/Trinity_out_S1s &>> Trinity_S1s_pbs.log
mkdir /lfs/maskerwei/RSEM2_Trinity_S1s
cd /lfs/maskerwei/RSEM2_Trinity_S1s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../Trinity_out_S1s/Trinity.fasta > Trinity_S1s.gene_trans_map 2> LOGs/trans_map_S1s.log
bowtie2-build --threads 64 ../Trinity_out_S1s/Trinity.fasta Trinity_S1s &> LOGs/bowtie2-build_S1s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S1s.gene_trans_map ../Trinity_out_S1s/Trinity.fasta Trinity_S1s &> LOGs/RSEM2_prep_ref_S1s.log
rsem-calculate-expression -p 64 --bowtie2 --paired-end \
	/lfs/maskerwei/FASTQs/Ir3633_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R1.fq.gz \
	/lfs/maskerwei/FASTQs/Ir3633_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3634_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3635_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S1_R2.fq.gz \
	Trinity_S1s RSEM2_S1s &> LOGs/RSEM2_Cal_Exp_S1s.log
