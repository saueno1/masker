#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_Velvet_S2s.out 
#PBS -e qSmall_Velvet_S2s.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Vh_S2s
cd /lfs/maskerwei/Velvet
#velveth S2s 31,45,4 -create_binary -separate -fastq.gz \
#    -shortPaired \
#        /lfs/maskerwei/FASTQs/Ir3636_R1.fq.gz   /lfs/maskerwei/FASTQs/Ir3636_R2.fq.gz \
#    -shortPaired2 \
#        /lfs/maskerwei/FASTQs/Ir3637_R1.fq.gz   /lfs/maskerwei/FASTQs/Ir3637_R2.fq.gz \
#    -shortPaired3 \
#        /lfs/maskerwei/FASTQs/Ir3638_R1.fq.gz   /lfs/maskerwei/FASTQs/Ir3638_R2.fq.gz \
#    -shortPaired4 \
#        /lfs/maskerwei/FASTQs/MiSeq_S2_R1.fq.gz /lfs/maskerwei/FASTQs/MiSeq_S2_R2.fq.gz
velvetg S2s_31 -read_trkg yes
oases S2s_31 -min_pair_count 10 -min_trans_lgth 300
velvetg S2s_35 -read_trkg yes
oases S2s_35 -min_pair_count 10 -min_trans_lgth 300
velvetg S2s_39 -read_trkg yes
oases S2s_39 -min_pair_count 10 -min_trans_lgth 300
velvetg S2s_43 -read_trkg yes
oases S2s_43 -min_pair_count 10 -min_trans_lgth 300
