#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=40
#PBS -l memsz_job=200GB 
#PBS -o qMedB_S2s.out 
#PBS -e qMedB_S2s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS2s 
#cd /home/maskerwei/RNA-seq/ms1/PC
#Trinity --CPU 40 --max_memory 200G --seqType fq \
#  --samples_file grp_S2_FQs.txt \
#  --min_contig_length 500 \
#  --output /lfs/maskerwei/Trinity_out_S2s &>> Trinity_S2s_pbs.log
#mkdir /lfs/maskerwei/RSEM2_Trinity_S2s
cd /lfs/maskerwei/RSEM2_Trinity_S2s
#/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../Trinity_out_S2s/Trinity.fasta > Trinity_S2s.gene_trans_map
#bowtie2-build --threads 40 ../Trinity_out_S2s/Trinity.fasta Trinity_S2s
#rsem-prepare-reference  --transcript-to-gene-map Trinity_S2s.gene_trans_map ../Trinity_out_S2s/Trinity.fasta Trinity_S2s
#rsem-calculate-expression -p 40 --bowtie2 --paired-end \
#	/lfs/maskerwei/FASTQs/Ir3636_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R1.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R1.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R1.fq.gz \
#	/lfs/maskerwei/FASTQs/Ir3636_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3637_R2.fq.gz,/lfs/maskerwei/FASTQs/Ir3638_R2.fq.gz,/lfs/maskerwei/FASTQs/MiSeq_S2_R2.fq.gz \
#	Trinity_S2s RSEM2_S2s
samtools sort -T S2s.tmp -@ 64 -m 3000M -l 9 -o RSEM_S2s.bam RSEM2_S2s.transcript.bam &> LOGs/sam_sort_RSEM2_S2s.log
samtools index -@ 64 RSEM_S2s.bam
