#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o qSingle_callVar_DBLib-CJ3006NRE.out 
#PBS -e qSingle_callVar_DBLib-CJ3006NRE.err 
#PBS -M maskerwei@affrc.go.jp
#PBS -N CVDBLib-CJ3006NRE
cd /lfs/maskerwei/EviGene
callVar.bsh DBLib CJ3006NRE
