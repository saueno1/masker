#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=64
#PBS -l memsz_job=200GB 
#PBS -o qMedB_S4s.out 
#PBS -e qMedB_S4s.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N TrinS4s 
#cd /home/maskerwei/RNA-seq/ms1/PC
#Trinity --CPU 64 --max_memory 200G --seqType fq \
#  --samples_file grp_S4_FQs.txt \
#  --min_contig_length 500 \
#  --output /lfs/maskerwei/Trinity_out_S4s &>> Trinity_S4s_pbs.log
mkdir /lfs/maskerwei/RSEM2_Trinity_S4s
cd /lfs/maskerwei/RSEM2_Trinity_S4s
mkdir LOGs
/home/maskerwei/bin/trinityrnaseq-Trinity-v2.4.0/util/support_scripts/get_Trinity_gene_to_trans_map.pl ../Trinity_out_S4s/Trinity.fasta > Trinity_S4s.gene_trans_map 2> LOGs/trans_map_S4s.log
bowtie2-build --threads 64 ../Trinity_out_S4s/Trinity.fasta Trinity_S4s &> LOGs/bowtie2-build_S4s.log
rsem-prepare-reference  --transcript-to-gene-map Trinity_S4s.gene_trans_map ../Trinity_out_S4s/Trinity.fasta Trinity_S4s &> LOGs/RSEM2_prep_ref_S4s.log
rsem-calculate-expression -p 64 --bowtie2 --paired-end \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R1.fq.gz \
    /lfs/maskerwei/FASTQs/MiSeq_S4_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R2.fq.gz \
    /lfs/maskerwei/RSEM2_Trinity_S4s/Trinity_S4s \
    RSEM2_S4s &> LOGs/RSEM2_Cal_Exp_S4s.log
samtools -T S4s.tmp -@ 64 -m 3000 -l 9 -o RSEM_S4s.bam RSEM2_S4s.transcript.bam &> LOGs/sam_sort_RSEM2_S4s.log
samtools index -@ 64 RSEM_S4s.bam
