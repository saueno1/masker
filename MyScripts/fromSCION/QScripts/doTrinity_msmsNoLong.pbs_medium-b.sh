#!/bin/bash
#PBS -q medium-b
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=30
#PBS -l memsz_job=200GB 
#PBS -o qMedB_ms1.out 
#PBS -e qMedB_ms1.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Trinity_m
cd /home/maskerwei/RNA-seq/ms1/PC
echo ${HOSTNAME} > trinity_MiSeq_msms_pbs_MedB.log
Trinity --seqType fq --max_memory 200G --left /lfs/maskerwei/FASTQs/MiSeq_S4_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R1.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R1.fq.gz --right /lfs/maskerwei/FASTQs/MiSeq_S4_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir718_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir719_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir720_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir721_R2.fq.gz,/lfs/maskerwei/FASTQs/HiSeq_Ir722_R2.fq.gz --CPU 30 --min_contig_length 500 --output /lfs/maskerwei/trinity_out_ms1 &>> trinity_MiSeq_msms_pbs.log
cat /lfs/maskerwei/trinity_out_ms1/Trinity.fasta | bzip2 -c9 > Trinity_out_ms1.fa.bz2
