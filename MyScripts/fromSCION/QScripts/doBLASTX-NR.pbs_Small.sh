#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=50GB 
#PBS -o qSmall_BLASTX-NR_Q000.out 
#PBS -e qSmall_BLASTX-NR_Q000.err 
#PBS -M maskerwei@affrc.go.jp 
#PBS -N BLNRQ000
cd /lfs/maskerwei/crossLibs/toSwissProt_Orth
blastx -query Q000.fa -db nr -task blastx-fast \
  -outfmt "6 std qlen slen stitle staxid" \
  -max_target_seqs 3 -num_threads 28 -out Q000-nr.txt
