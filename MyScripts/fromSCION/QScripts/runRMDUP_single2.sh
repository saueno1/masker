#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o Single_RMDUP_QLib-DBLib.out 
#PBS -e Single_RMDUP_QLib-DBLib.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N RMDUPQLibDBLib
cd /lfs/maskerwei/Trinity_out_DBLib
mv Trinity_QLib-DBLib_NI.bam QLib-DBLib_NI.ud.bam && samtools rmdup --reference DBLib.fa QLib-DBLib_NI.ud.bam QLib-DBLib_NI.bam
samtools index QLib-DBLib_NI.bam
