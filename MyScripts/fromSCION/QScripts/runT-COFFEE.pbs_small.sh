#!/bin/bash
#PBS -q small
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=28
#PBS -l memsz_job=110GB 
#PBS -o qSmall_MYB_APBCac.out 
#PBS -e qSmall_MYB_APBCac.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N Tcoffee
cd /lfs/maskerwei/pickESTs/toOrthCand/orthologs/MYB/T-COFFEE
t_coffee -in MYB_APBC.fa -run_name MYB_APBCac -mode accurate \
  -pdb_db /lfs/maskerwei/BLASTDB/pdb.pep -blast LOCAL \
  -output clustalw_aln,msf_aln,fasta_aln,phylip,score_ascii \
  -multi_core 28 -n_core 28 -quiet MYB_APBCac.log
