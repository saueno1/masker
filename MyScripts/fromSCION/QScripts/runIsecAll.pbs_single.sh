#!/bin/bash
#PBS -q single
#PBS -l elapstim_req=600:00:00 
#PBS -l cpunum_job=1
#PBS -l memsz_job=4GB 
#PBS -o Single_IsecAll_DBLib.out 
#PBS -e Single_IsecAll_DBLib.err
#PBS -M maskerwei@affrc.go.jp 
#PBS -N IsecDBLib
cd /lfs/maskerwei/EviGene/tenBAMs_BWA/VCFs
if [ ! -d LOGs ] ; then
 mkdir LOGs
fi
if [ -d ISEC ] ; then
 mv ISEC ISEC_bak${JOBID}
fi
bash ./isecAll.bsh &> LOGs/IsecAll.log
