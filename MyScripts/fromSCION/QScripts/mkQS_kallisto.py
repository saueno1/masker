#!/usr/bin/python
'''
Make queue scripts by the content of grp_GroupName_FQs.txt, which is four columns, main group name, subgroup name, left/single reads filename, right reads filename (if have).
'''
import sys
from subprocess import call

tmpQSH="tmp_kallisto.pbs_small.sh"
with open(sys.argv[1],"r") as f :
   for l in f :
     
     with open(tmpQSH,"w") as wf :
       lA=l.rstrip("\n").split('\t')
       libNm=lA[2][:lA[2].find("_t")]
       spec = lA[0][:lA[0].find("MO")] if "MO" in lA[0] else lA[0][:lA[0].find("LF")]
       if len(lA) < 4 or lA[3]=="" :
          Sg=" --single -l 75 -s 40"
       else :
          lA[2]=lA[2]+" "+lA[3]
          Sg=""
       wf.write("#!/bin/bash\n")
       wf.write("#PBS -q small\n")
       wf.write("#PBS -l elapstim_req=600:00:00\n")
       wf.write("#PBS -l cpunum_job=28\n")
       wf.write("#PBS -l memsz_job=50GB\n")
       wf.write("#PBS -o qSmall_Kallisto_"+libNm+".out\n")
       wf.write("#PBS -e qSmall_Kallisto_"+libNm+".err\n")
       wf.write("#PBS -M maskerwei@affrc.go.jp\n")
       wf.write("#PBS -N KLS_"+libNm+"\n")
       wf.write("cd /lfs/maskerwei/compare/"+spec+"\n")
       wf.write("kallisto quant -i "+spec+".idx -o "+lA[1]+Sg+" --bias -b 100 --seed=28 --threads=28 "+lA[2]+"\n")
     call(["qsub",tmpQSH])
