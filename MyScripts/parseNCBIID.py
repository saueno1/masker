#!/usr/bin/python
import sys

IDtab={}
with open(sys.argv[1],"r") as FA :
    for l in FA :
       if l[0] != ">" :
         print l.rstrip()
         continue
       lA= l[1:].rstrip("\n").split(" ",1)
       t=lA[1][lA[1].find("[")+1:lA[1].find("]")]
       t=t.replace(" ","_")
       t=t.replace("var.","var")
       t=t.replace("subsp.","subsp")
       g = " ".join(lA).replace(" ","_") 
       g=g.replace("__","_")
       g=g.replace("[","")
       g=g.replace("]","")
       IDtab[g]=t+"_"+lA[0]
       print(">"+IDtab[g])

#with open(sys.argv[2],"r") as TG :
#    for l in TG :
#       l=l.rstrip("\n")
#       for o,n in IDtab.iteritems() :
#          l=l.replace(o,n)
 #      print l+"\n"
