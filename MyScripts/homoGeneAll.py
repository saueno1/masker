#!/usr/bin/python
import os,sys,gzip

minSc=200.0
minPLen=0.5
hmGT={}
hmG={}

def parseAlign(Qname,Sname,Brlt) :
   with gzip.open(Brlt,'rb') as f:
       for line in f :
           line=line.rstrip("\n")
           lA=line.split("\t")
           Qgene=lA[0][:lA[0].find('_i')]
           Sgene=lA[1][:lA[1].find('_i')]
           Sc=float(lA[11])
           if Sc >= minSc and ( float(lA[3])/float(lA[12]) >= minPLen or float(lA[3])/float(lA[13]) >= minPLen )  :
             if Qgene not in hmG :
                hmG[Qgene]=[Sgene,Sc]
             else :
                ''' for one query, keep only one best suject '''
                if hmG[Qgene][1] < Sc :
                   hmG[Qgene] = [Sgene,Sc ]
   return hmG

for fn in sorted(os.listdir("./")) :
   if "_dcmega_" in fn and fn.endswith(".txt.gz") :
      fnm=fn
      fnPre="Trinity_"
      fnPost="_dcmega"
      QS=fnm[len(fnPre):fnm.find(fnPost)].split("-")
      if QS[0] not in hmGT :
         hmGT[QS[0]]={ QS[1] : parseAlign(QS[0],QS[1],fnm) }
      else :
         hmGT[QS[0]][QS[1]]=parseAlign(QS[0],QS[1],fnm)
#      print fnm,len(hmGT[QS[0]][QS[1]])

Qlst = sorted(hmGT)

for QLp in range(len(Qlst)-1) :
    SLp=QLp+1
    QL=Qlst[QLp]
    SL=Qlst[SLp]
    for QG,SGA in hmGT[QL][SL].iteritems() :
       if SGA[0] not in hmGT[SL][QL] or QG != hmGT[SL][QL][SGA[0]][0] :
          print QL,SL,QG,SGA[0]

