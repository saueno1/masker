#!/usr/bin/python
# usage: getLORF.py DNA.FASTA
# Read DNA FASTA file, then extract longest ORF and output as a protein sequence
# The output file : translated ORF in FASTA
# The functions, get_longest and get_orfs were copy from Patrick Maupin's reply on https://stackoverflow.com

import sys,re
import string
import subprocess

def revClSeq(dna) :
    complement = { 'A':'T', 'C':'G', 'G':'C', 'T':'A' }
    return ''.join([complement[base] for base in dna[::-1]])

StCodon, EdCodon = [re.compile(x) for x in 'ATG TAG|TGA|TAA'.split()]

revtrans = string.maketrans("ATGC","TACG")


codontable = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

VT={}
VSum={}

def get_longest(starts, ends):
    ''' Simple brute-force for now.  Optimize later...
        Given a list of start locations and a list
        of end locations, return the longest valid
        string.  Returns tuple (length, start position)

        Assume starts and ends are sorted correctly
        from beginning to end of string.
    '''
    results = {}
    # Use smallest end that is bigger than each start
    ends.reverse()
    for start in starts:
        for end in ends:
            if end > start and (end - start) % 3 == 0:
                results[start] = end + 3
    results = [(end - start, start) for
               start, end in results.iteritems()]
    return max(results) if results else (0, 0)

def get_orfs(dna):
    ''' Returns length, header, forward/reverse indication,
        and longest match (corrected if reversed)
    '''
    header, seqf = dna
    seqr = seqf[::-1].translate(revtrans)
    def readgroup(seq, group):
        return list(x.start() for x in group.finditer(seq))
    f = get_longest(readgroup(seqf, StCodon), readgroup(seqf, EdCodon))
    r = get_longest(readgroup(seqr, StCodon), readgroup(seqr, EdCodon))
    (length, index), s, direction = max((f, seqf, '+'), (r, seqr, '-'))
    return length, header, index, direction, s[index:index + length]

def trans_orf(dna):
    header, seqf = dna
    aa=""
    for i in range(0, len(seqf), 3) :
       aa+=codontable[seqf[i:i+3]]
    return aa

# Start of main scripts
DNAFA=sys.argv[1]
with open(DNAFA,"r") as D :
    data=D.read()
fnPre=DNAFA[:DNAFA.find(".fa")]
#OCForf=open(fnPre+"_ORFAA.fa","w")
OCA={}

data = [ x.split('\n',1) for x in data.split('>')]
data = [(x[0],''.join(x[1].split())) for x in data if len(x) ==2 ]

for seq in data :
   seqID=seq[0].split()
   LongOrf=get_orfs((seqID[0],seq[1]))
   if len(LongOrf[4]) > 0 :
      OrfSt = LongOrf[2]
      OrfDir = LongOrf[3]
      OrfEd = int(LongOrf[2])+int(LongOrf[0])-1
#      OCForf.write(" ".join([">"+seqID[0],str(OrfSt),str(OrfEd),str(OrfDir)])+"\n"+trans_orf((LongOrf[1],LongOrf[4]))+"\n")
      OrfAA=trans_orf((LongOrf[1],LongOrf[4]))
      if OrfAA[-1]=="_" :
         OrfAA=OrfAA[:-1]
      print " ".join([">"+seqID[0],str(OrfSt),str(OrfEd),str(OrfDir),str(len(seq[1]))])
      print OrfAA

D.close()
#OCForf.close()
