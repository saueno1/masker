#!/usr/bin/python
import sys
import gzip

minR=0.0
BLASTrltD={}
BLASTrltF=gzip.open(sys.argv[1],"rb")

def rmOv(HSPs) :
    HSPsR=[]
    StP = -1
    EdP = -1
    for st,ed in sorted(HSPs):
        if st > EdP :
           HSPsR.append( ( st,ed ) )
           StP,EdP = st, ed
        else :
           EdP = max(EdP, ed)
           HSPsR[-1]= (StP, EdP)
    return HSPsR

for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    QId=Rec[0]
    SId=Rec[1]
    QSA=[(),() ] 
    for QS in ( 0,1) :
        lnum,rnum = QS*2+6, QS*2+7
        if int(Rec[lnum]) < int(Rec[rnum]) :
           Str='+'
           QSA[QS]=( ( int(Rec[lnum]), int(Rec[rnum]) ))
        else :
           Str='-'
           QSA[QS]=( ( int(Rec[rnum]), int(Rec[lnum]) ) )
    if QId not in BLASTrltD :
       BLASTrltD[QId]={ SId : [ [ QSA[0] ], [ QSA[1] ],  Rec[12:]  ] }
    else :
       if SId not in BLASTrltD[QId] :
          BLASTrltD[QId][SId]=[ [ QSA[0] ], [ QSA[1] ],  Rec[12:]  ]
       else :
          msA=BLASTrltD[QId][SId]
          for QS in ( 0, 1 ) :
              msA[QS].append(QSA[QS])
              BLASTrltD[QId][SId][QS]=rmOv(msA[QS])

for Q,SRec in sorted(BLASTrltD.iteritems())  :
   HSPs=[[],[]]
   for S,(HSPs[0],HSPs[1],QSi) in sorted(SRec.iteritems()) :
      HSPlen=[0,0]
      for QS in ( 0, 1 ) :
         for HSP in HSPs[QS] :
             HSPlen[QS]+= HSP[1]-HSP[0]+1
      if HSPlen[0]/float(QSi[0]) >= minR or HSPlen[1]/float(QSi[1]) >= minR :
         print "\t".join([ Q,S,str(HSPlen[0]),str(HSPlen[1]),str(HSPlen[0]/float(QSi[0])),str(HSPlen[1]/float(QSi[1]))])
