#!/usr/bin/python
import sys

RepMOut=open(sys.argv[1],"r")
RSEMF=open(sys.argv[2],"r")
RMOs={}

with open(sys.argv[1]) as RepMOut :
   for _ in xrange(3) :
      next(RepMOut)
   for rec in RepMOut :
      rec=rec.rstrip("\n")
      tmpA=rec.split()
      CtgG=tmpA[4][:tmpA[4].find('_i')]
      if CtgG not in RMOs :
         RMOs[CtgG]=[]
      RMOs[tmpA[4][:tmpA[4].find('_i')]].append(tmpA)

for RSEMl in RSEMF :
   RSEMl=RSEMl.rstrip("\n")
   recRSEM=RSEMl.split("\t")
   CtgG=recRSEM[0]
   if CtgG in RMOs :
      RMOs[CtgG].sort(key=lambda x: x[0], reverse=True)
      print "\t".join([CtgG,recRSEM[-2],recRSEM[-1]]+RMOs[CtgG][0][9:11]+[RMOs[CtgG][0][0]])
