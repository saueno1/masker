#!/usr/bin/python
import sys
import gzip
import time

st=time.time()
minR=0.4
ESTid_pre="EST2906a"
# minimium overlapped base pairs : minOv=20
BLASTrltF=gzip.open(sys.argv[1],"rb")
BLASTrltD={}
OrtUG={}
OrtGS={}
canEST={}
SwProG={}

# structure of BLASTrltD[ContigName]=
#              [ [ most part of BLAST result, start from "subjectID". Total 14 fields ], 
#                [ max of Iden sum length of HSP ] ]
for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    if Rec[0] not in BLASTrltD :
       BLASTrltD[Rec[0]]=[ Rec[1:], [ float(Rec[2]),int(Rec[3]) ] ]
    else :
       if BLASTrltD[Rec[0]][0][0] == Rec[1] :
          BLASTrltD[Rec[0]][1][1] += int(Rec[3])
          if BLASTrltD[Rec[0]][1][0] < float(Rec[2]) : 
             BLASTrltD[Rec[0]][1][0] = float(Rec[2])

for CtgID,rec in sorted(BLASTrltD.items(), key=lambda x : x[0] ):
# If the joined record match to the condition, eg: sum length of 
# HSP could cover over minR of subject length.
# rec[0] : Start from "subjectID". It's most part of BLAST result. Total 14 fields.
# rec[1] : [ max of Iden, sum length of HSP ]
    if float(rec[1][1])/float(rec[0][12]) < minR or \
         ( rec[0][0] in OrtGS and float(OrtGS[rec[0][0]]) > float(rec[0][10]) ) :
       continue
    HmG=CtgID[:CtgID.find("_i")]
# the structure of OrtUG[HmG]={ geneID: [CtgID, rec[0][1:]]  } :
# HmG : Homologous group name by 1st meet group without other homologous
# CtgID : contig ID
# rec[0][1:] : start from Iden. Most part of BLAST result. Total 13 fields
    if HmG in OrtUG :
       if rec[0][0] not in OrtUG[HmG] or \
            float(OrtUG[HmG][rec[0][0]][10]) < float(rec[0][10]) :
          OrtGS[rec[0][0]]=float(rec[0][10])
          OrtUG[HmG][rec[0][0]]=[CtgID]+rec[0][1:]
    else :
       HmG2='x'
       for k,i in OrtUG.iteritems() :
           if ( rec[0][0] in i and float(i[rec[0][0]][10]) < float(rec[0][10]) ) \
                or HmG in (x[0][:x[0].find("_i")] for x in i.values() ) :
              HmG2=k
              OrtGS[rec[0][0]]=float(rec[0][10])
              OrtUG[k][rec[0][0]]=[CtgID]+rec[0][1:]
              break
       if HmG2== 'x' :
          OrtGS[rec[0][0]]=float(rec[0][10])
          OrtUG[HmG]={ rec[0][0]: [CtgID]+rec[0][1:] }

# Only print out one contig for one gene in SwissProt
# The highest score one.
# The out information of GeneID in SwissProt, contig ID, 
#           contig length(Nucl.), Gene length(a.a), annotation of SwissProt gene
SerNo=1
for HmG,HmGs in OrtUG.iteritems() :
# The "key" is the hitted subject (gene) ID
# The content of "rec" are [0]"contig ID",
#  [1][0]"Identity", [1][1]"HSP length", [1][2]"mismatch #",
#  [1][3]"gap #", [1][4]"start of query (contig)", 
#  [1][5]"end of query (contig)", [1][6]"start of subject (gene)", [1][7]"end of subject (gene)",
#  [1][8]"evalue", [1][9]"bitscore", [1][10]"query length", 
#  [1][11]"subject length", [1][12]"subject title (annotation)"
#   for CtgRec in sorted( HmGs.items(), 
#		key=lambda x: ( float(x[1][10]), float(x[1][1]), float(x[1][11]) ), 
#		reverse=True ) :
   CtgRec = sorted( HmGs.items(), 
		key=lambda x: ( float(x[1][10]), float(x[1][1]), float(x[1][11]) ), 
		reverse=True )[0]
# output : matched gene ID, contig ID (with library ID), 
#	HSP length, bitscore, query length,
#	subject length, subject title
#       print('\t'.join([ESTid,key,CtgRec[0]]+[CtgRec[1][1]]+CtgRec[1][9:]))

#       CtgG=CtgRec[0][:CtgRec[0].find("_i")]
#       if CtgG not in canEST :
#          canEST[CtgG]={}

#       canEST[CtgRec[0]]=CtgRec[1][0:2]+CtgRec[1][9:]
   canEST[CtgRec[0]]=CtgRec[1]
# Because the inner list has been sorted by bit-score, 
# HSP length and contig length in descending order, 
# we can break the loop after print the 1st one.
#       break
lastCtgH="x"
for CtgG,hitGs in sorted(canEST.items(), 
	key=lambda x : ( x[1][0][:x[1][0].find("_i")], -float(x[1][10]) ) ):
#   for HitG in sorted(hitGs.items(), key=lambda x: x[1][2], reverse=True ) :
    CtgH=hitGs[0][:hitGs[0].find("_i")]
    if CtgH != lastCtgH :
       ESTid=ESTid_pre+str(SerNo).zfill(5)
       print('\t'.join([ESTid,CtgG]+hitGs))
       SerNo+=1
    lastCtgH=CtgH

BLASTrltF.close()
