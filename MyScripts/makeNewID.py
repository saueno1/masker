#!/bin/python
import sys,re

if len(sys.argv) > 2 :
  preNm=sys.argv[1]
  inFa=open(sys.argv[2],"r")
else :
  preNm="SpVar"
  inFa=sys.stdin
GI={}
for FaLine in inFa :
  FaLine=FaLine.rstrip("\n")
  if FaLine[0] == ">" :
     OId=FaLine[1:].split()[0]
     OIdG=OId[:OId.find("_i")]
     if OIdG not in GI :
        GI[OIdG]=[]
     GI[OIdG].append(OId[OId.find("_i")+2:])
g=0
for k in sorted(GI) :
   g+=1
   for i in GI[k] :
      print "\t".join([k+"_i"+i,preNm+"_EST"+str(g).zfill(6)+"-"+i.zfill(2)])
