#!/usr/bin/python
import sys
import gzip

minR=0.4
# minimium overlapped base pairs : minOv=20
BLASTrltF=gzip.open(sys.argv[1],"rb")
BLASTrltD={}
OrtUG={}

for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    if Rec[0] not in BLASTrltD :
       BLASTrltD[Rec[0]]=[ Rec[1:], [ float(Rec[2]),int(Rec[3]) ] ]
    else :
       if BLASTrltD[Rec[0]][0][0] == Rec[1] :
          BLASTrltD[Rec[0]][1][1] += int(Rec[3])
          if BLASTrltD[Rec[0]][1][0] < Rec[2] : 
             BLASTrltD[Rec[0]][1][0] = Rec[2] 

for key,rec in BLASTrltD.iteritems()  :
    x=[key]+rec[0]+[str(rec[1][0]),str(rec[1][1])]
# If the joined record match to the condition, eg: sum length of HSP could explain over minR of subject length probably
    if float(x[16])/float(x[13]) >= minR :
## print these column: query ID, subject ID, sum length of HSP, query length, subject length, title of subject
##       print("\t".join(x[:-2]))
       if x[1] not in OrtUG :
          OrtUG[x[1]]={}
       OrtUG[x[1]][x[0]]=x[2:-2]

# Only print out one contig for one gene in SwissProt
# The longest length one.
# The out information of GeneID in SwissProt, contig ID, contig length(Nucl.), Gene length(a.a), annotation of SwissProt gene
for key,rec in OrtUG.iteritems() :
   for CtgRec in sorted(rec.items(), key=lambda x: ( float(x[1][9]), float(x[1][1]), float(x[1][10]) ), reverse=True) :
       print('\t'.join([key,CtgRec[0]]+[CtgRec[1][1]]+CtgRec[1][9:]))
       break

BLASTrltF.close()
