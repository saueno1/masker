#!/usr/bin/python
import sys
import gzip

minR=0.4
BLASTrltD={}
BLASTrltF=gzip.open(sys.argv[1],"rb")

def mergeSelf(Arr=[]) :
   '''
   Arr=[ [ Qst, Qed, Sst, Sed ] x n ]
   '''
   if len(Arr) < 2 :
      return Arr
   QArr=sorted(Arr)
   RArr=[]
   for i in range(0,len(Arr)-1) :
       Qst=QArr[i][0]
       Qed=QArr[i][1]
       Sst=QArr[i][2]
       Sed=QArr[i][3]
       msA=1
       for j in range(i+1,len(Arr)) :
          if Qst <= QArr[j][0]  <= Qed or Qst <= QArr[j][1] <= Qed or QArr[j][0] <= Qst <= QArr[j][1] or QArr[j][0] <= Qed <= QArr[j][1] :
                Qst = Qst if Qst < QArr[j][0] else QArr[j][0]
                Qed = Qed if Qed > QArr[j][1] else QArr[j][1]
                Sst = Sst if Sst < QArr[j][2] else QArr[j][2]
                Sed = Sed if Sed > QArr[j][3] else QArr[j][3]
                RArr.append([ Qst,Qed,Sst,Sed ])
                msA=0
                break
       if msA != 0 :
          RArr.append([ Qst,Qed,Sst,Sed ])           
   while len(mergeSelf(RArr)) != len(RArr) :
      RArr=mergeSelf(RArr) 
   return RArr

for line in BLASTrltF :
    line=line.rstrip("\n")
    Rec=line.split("\t")
    QId=Rec[0]
    SId=Rec[1]
    Qst=int(Rec[6])
    Qed=int(Rec[7])
    if int(Rec[8]) < int(Rec[9]) :
       Str='+'
       Sst = int(Rec[8])
       Sed = int(Rec[9])
    else :
       Str='-'
       Sst = int(Rec[9])
       Sed = int(Rec[8])
    if QId not in BLASTrltD :
       BLASTrltD[QId]={ SId : [ [ [Qst,Qed,Sst,Sed] ],Rec[12:] ] }
    else :
       if SId not in BLASTrltD[QId] :
          BLASTrltD[QId][SId]=[ [ [Qst,Qed,Sst,Sed] ],Rec[12:] ]
       else :
         msA=BLASTrltD[QId][SId][0]
         for msi in range(0,len(msA)) :
            if Qst <= msA[msi][0]  <= Qed or Qst <= msA[msi][1] <= Qed or msA[msi][0] <= Qst <= msA[msi][1] or msA[msi][0] <= Qed <= msA[msi][1] :
                Qst = Qst if Qst < msA[msi][0] else msA[msi][0]
                Qed = Qed if Qed > msA[msi][1] else msA[msi][1]
                Sst = Sst if Sst < msA[msi][2] else msA[msi][2]
                Sed = Sed if Sed > msA[msi][3] else msA[msi][3]
                BLASTrltD[QId][SId][0][msi]=[ Qst,Qed,Sst,Sed ]
                msA=0
                break
         if msA != 0 :    
            BLASTrltD[QId][SId][0].append([ Qst,Qed,Sst,Sed ])


for Q,SRec in sorted(BLASTrltD.iteritems())  :
   for S,(HSPs,QSi) in sorted(SRec.iteritems()) :
       HSPs=mergeSelf(HSPs)
       HSPcov=0
       for HSP in HSPs :
           HSPcov+= HSP[3]-HSP[2]+1
       if HSPcov/float(QSi[1]) >= minR :
          print "\t".join([ Q,S,str(HSPcov),str(HSPcov/float(QSi[1]))])

