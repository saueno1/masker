#!/usr/bin/python
import sys

if len(sys.argv) > 1 :
  inFa=open(sys.argv[1],"r")
else :
  inFa=sys.stdin

seqA={}
sn=0
sp=2000
spfPre="splitFA_"
data = inFa.read()
data = [ x.split('\n',1) for x in data.split('>')]
data = [(x[0],''.join(x[1].split())) for x in data if len(x) ==2 ]

for seq in data :
   seqA[seq[0].split()[0]]=seq[1]

print len(data)

for (seqID,seq) in sorted(seqA.iteritems()) :
   if sn%sp == 0 :
      if 'spF' in locals() and not spF.closed :
         spF.close()
      spF=open(spfPre+str(sn/sp).zfill(3),"w")
   spF.write(">"+seqID+"\n"+seq+"\n")
   sn+=1

if not spF.closed :
   spF.close()
