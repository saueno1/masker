#!/usr/bin/python
import sys,gzip

GG={}

with gzip.open(sys.argv[1],"rb") as f :
    for l in f :
        if l[0]=="#" :
          continue
        GFFr=l.rstrip(";\n").split("\t")
#        Gid=GFFr[0][:GFFr[0].find("_i")]
        if GFFr[1] != "Pfam" :
           continue
        Gid=GFFr[0]
        GFFatr=GFFr[8].split(";")
        for [k,v] in [ i.split("=") if "=" in i else i.split() for i in GFFatr] :
           k=k.lstrip(" ")
           if k=="Name" :
              GOs=set([ i.strip("\"") for i in v.split(",")])
              if Gid not in GG :
                 GG[Gid]=GOs
              else :
                 GG[Gid].update(GOs)

for k,v in sorted(GG.iteritems()) :
   for i in v:
      print "\t".join([k,i])
