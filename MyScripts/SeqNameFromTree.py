#!/usr/bin/python
# usage: ./SeqNameFromTree.py MYB4R2ac_ML_cons.tree MYB4R2ac.aln.fasta
import sys,re

Ss=re.compile('\[[^\]]+\]')
SnameTab={}
OTreeID={}
UpP=re.compile('\([^(,)]+')
DwP=re.compile(',[^(,)]+')
f=open(sys.argv[1],"r")
Tree=f.readline()
m1=UpP.findall(Tree)
m2=DwP.findall(Tree)
for i in m1+m2 :
   A=i.strip("(,)")
   if A[:3] == "XP_" or A[:3]== "MA_" or A[:3] == "NP_" :
      tmp=A.split("_")
      k=tmp[0]+"_"+tmp[1]
   elif ":" in A :
      A2=A.strip("'").split(" ")
      k=A2[0]+"_"+A2[1] if A2[0][:2] == "XP" or A2[0][:4] == "PITA" else A2[0]
   else :
      k=A.split("_")[0]
   OTreeID[k]=[A]


with open(sys.argv[2],"r") as f :
    for l in f :
       if l[0] != ">" : continue
       A=l.lstrip(">").split("  ",1)
       m=Ss.findall(l)
       if m :
         sname=m[0].strip("[]").replace(" ","_")
         sname=sname.replace(":","_")
       else :
         sname=""
       SName=sname+"_"+A[0]
       OTreeID[A[0]].extend([SName])
#       print("\t".join([A[0],SName]))

for k,i in OTreeID.iteritems() :
   Tree=Tree.replace(i[0],i[1])

print Tree
