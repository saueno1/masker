#!/usr/bin/python
'''
Author	:	Fu-Jin Wei
Date	:	Sep. 22, 2017 (original on Sep.6, 2017)
command	:	getParaG.py BestHitCtgByGenes.txt
require	:	t_coffee (path to )
descript	:
  BestHitCtgBtGenes.txt is the four-columns table of 
template genes (or orthlogous candidate genes), source 
library of best hit contig, best hit contig ID and matched strand.
Since it take long time to process multiple alignment by T-COFFEE,
please do multiple alignment after having FASTA with paralogous contigs.
Output	:
  One FASTA of potential contigs from all libraries.
Changlog :
Sep.22,2017 : orientation!! wrong for others
Sep.14,2017 : add some parts to deal with added "strand"
'''
import sys,gzip,subprocess

libs=[ "S1s","S2s","S3s","S4s","S5s","S6s","Ooi7","S1NK4","S5HK7","S8HK5" ]

OrthGA={} # OrthGA : container for last reslt. mutiple layer of Dictionary.
openSr={} # openSr : container for library (source),  Ctg ID, matched gene
with open(sys.argv[1],"r") as BOGf :
   for GnC in BOGf :
      OCG,BOGS,BOG,strand = GnC.rstrip("\n").split("\t")
      if BOGS == "Ooi-7" : BOGS="Ooi7"
      if OCG not in OrthGA :
        OrthGA[OCG]={ BOGS : [ BOG, strand ] }
      if BOGS not in openSr :
        openSr[BOGS]={ BOG : OCG }
      else :
        openSr[BOGS][BOG]=OCG

for BOGS,BGr in sorted(openSr.iteritems()) :
      for l in libs :
        if l==BOGS :
          continue
        with gzip.open("Trinity_"+BOGS+"-"+l+"_dcmega_outfmt6_sel1.txt.gz","rb") as Brlt :
            matchSc={}
            for BRrl in Brlt :
               BRr=BRrl.rstrip("\n").split("\t")
               BRrStr="+" if BRr[8] < BRr[9] else "-"
               '''
                 Pick a highest score , then record Ctg ID and strand
               '''
               if BRr[0] in BGr and ( BRr[0] not in matchSc or matchSc[BRr[0]][2] < BRr[11] ):
                  '''
                      Because there is no sign for marking best orthologous contigs, 
                    it need to retrieve from the OrthGA. By comparing with the orientation 
                    of two result, to assign the "strand" for retreive sequence from the
                    BLAST databases.
                  '''
                  BRrStr="+" if BRrStr == OrthGA[openSr[BOGS][BRr[0]]][BOGS][1] else "-"
                  matchSc[BRr[0]]=[ BRr[1], BRrStr, BRr[11] ]
            for OCtg,PCtgA in matchSc.iteritems() :
                OrthGA[BGr[OCtg]][l]= PCtgA[:2] 
            

for g,PGs in sorted(OrthGA.iteritems()) :
   OrthCtgFN=g+"_OrthCtg.fa"
   MSAout=g+"_CtgMSA"
   g1=g if g.find(".") == -1 else g[:g.find(".")]
   MSAlog=g+"_MSA.log"
   print "Start to process "+g
   with open(OrthCtgFN,"w") as OGCf :
     for l,Ctg in sorted(PGs.iteritems()) :
        if l=="Ooi7" : l="Ooi-7"
        Qstr="plus" if Ctg[1]=="+" else "minus"
        cmd = "blastdbcmd -entry "+Ctg[0]+" -strand "+Qstr+" -db Trinity_"+l
        print cmd
        p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE )
        (out, err) = p.communicate()
        out=out.replace(">TRINITY",">"+l+"_TRINITY")
        (SeqID,Seq)=out.split("\n",1)
        SeqID=SeqID.split()[0]
        Seq=''.join(Seq.split())
        OGCf.write(SeqID+"\n"+Seq+"\n")
   ''' Stop to process multiple alignment in this program to save time
   signMSA=subprocess.call(['t_coffee','-in='+OrthCtgFN,
           '-mode=procoffee','-method=t_coffee_msa',
           '-output=clustalw_aln score_ascii','-maxnseq=150',
           '-maxlen=10000','-case=upper','-seqnos=off',
           '-outorder=input','-run_name='+g1,'-multi_core=28','-quiet='+MSAlog])

   if signMSA == 0 :
     print MSAout+" is done!"
   else :
     print "something wrong with alignment"
   '''
