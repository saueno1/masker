#!/usr/bin/python
import sys,subprocess

ORFf=sys.argv[1]
MaxMis=3
FnIdS="_OrthCtg"

if FnIdS in ORFf:
  OrGN = ORFf[:ORFf.find(FnIdS)]
else :
  OrGN = "unnameGene"

with open(ORFf,"r") as f :
    data=f.read()
data = [ x.split('\n',1) for x in data.split('>')]
data = [(x[0],''.join(x[1].split())) for x in data if len(x) ==2 ]

ORFFA={}
ComCtg=[]
grouped={}
ORFgrp=0

for seq in data :
   seqID=seq[0].split()[0]
   ( Lib,seqID0 ) = seqID.split("_",1)
   ORFFA[Lib]=[ seqID0, seq[1] ]
if len(ORFFA) < 2 :
   sys.exit("Sequence number less than 2") 
for Lib,seq in ORFFA.iteritems() :
   cmd = "echo "+seq[1]+" | blastp -query - -subject "+ORFf+" -outfmt '6 std qlen slen'"
   p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE )
   out, err = p.communicate()
   for l in out.rstrip("\n").split("\n") :
      bl=l.split("\t")
      if bl[1] == Lib+"_"+seq[0] : continue
      elif int(bl[4]) <= MaxMis and abs(int(bl[13])-int(bl[12])) <= MaxMis :
         sLib,sId=bl[1].split("_",1)
         if Lib not in (p[1] for p in ComCtg ) :
           ORFgrp+=1
           ComCtg.append([str(ORFgrp),Lib]+seq)
         if sLib not in (p[1] for p in ComCtg ) :
           ComCtg.append([str(ORFgrp),sLib,sId,ORFFA[sLib][1]])
if len(ComCtg) > 1 :
  for i in ComCtg :
    print "\t".join([OrGN]+i)
else :
  sys.exit("None reliable orthologous genes to be found for "+OrGN+", now.")
