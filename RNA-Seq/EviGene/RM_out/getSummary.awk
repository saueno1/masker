#!/bin/bash
awk ' 
BEGIN{ OFS="\t" }
{ 
  if(NF==15) { 
     print $5,$11 
  } else if(NF>15) { 
     print $5,$11 
  } } ' CJ3006NRE.fa.out | sort | uniq | awk '
  BEGIN{ lst=""; ldec=""; OFS="\t" } 
  { 
    if(lst==$1){ 
      ldec=ldec","$2 
    } else { 
      print lst,ldec ;
      ldec=$2 
    } 
    lst=$1 
  } 
  END { print lst,ldec } '
