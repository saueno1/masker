#!/usr/bin/python
import sys,pysam

GF=pysam.TabixFile("CJ3006NRE-IPS530.cDNA.gff3.gz")

GeneS = {} # Three items: total lengh, start of CDS, end of CDS
for row in GF.fetch() :
  if row[0]=="#" : 
    continue
  recA=row.rstrip("\n").split("\t")
  if recA[2]=="ORF" :
     GeneS[recA[0]] = [ int(recA[6]),int(recA[3]),int(recA[4]) ]

for g in sorted(GeneS) :
   OstrA=[]
   GRec=GeneS[g]
   Fr=str((GRec[1]-1)%3)
   colSSE=[".","+",Fr]
   Att="gene_id \""+g+"\"; transcript_id \""+g+".1\""
   OstrA.append([ g,"EviGene","CDS",str(GRec[1]),str(GRec[2]),".","+",Fr,Att])
   OstrA.append([ g,"EviGene", "start_codon",str(GRec[1]),str(GRec[1]+2),".","+",Fr,Att])
   if GRec[1] > 1 :
      OstrA.append([g,"EviGene","5UTR","1",str(GRec[1]-1),".","+",".",Att] )
   if GRec[2] < GRec[0] :
      OstrA.append([ g,"EviGene","3UTR",str(GRec[2]+1),str(GRec[0]),".","+",".",Att])
#   OstrA.append([ g,"EviGene", "stop_codon",str(GRec[2]+1),str(GRec[2]+3),".","+",".",Att])
   for l in OstrA :
      print("\t".join(l))
